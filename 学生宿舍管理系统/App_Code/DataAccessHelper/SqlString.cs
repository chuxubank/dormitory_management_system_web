﻿using System;
using System.Collections;

namespace StudentApartment.DataAccessHelper
{
    public class SqlString
    {
        public static String GetSafeSqlString(String Str)
        {
            return Str.Replace("'", "''");
        }

        public static String GetQuotedString(String Str)
        {
            return ("'" + GetSafeSqlString(Str) + "'");
        }
    }

}

