﻿using System;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using StudentApartment.DataAccessLayer;
using StudentApartment.DataAccessHelper;



namespace StudentApartment.BusinessLogicLayer
{
    public class Admin
    {
        private string adminUserName; 
        private string adminPassword; 
        private string errMessage;   

        #region MyRegion
        public string AdminUserName
        {
            set
            {
                this.adminUserName = value;
            }
            get
            {
                return this.adminUserName;
            }
        }

        public string AdminPassword
        {
            set
            {
                this.adminPassword = value;
            }
            get
            {
                return this.adminPassword;
            }
        }

        public string ErrMessage
        {
            set
            {
                this.errMessage = value;
            }
            get
            {
                return this.errMessage;
            }
        }
        #endregion
        

        public bool checkAdmin() {
            string queryString;
            bool user;
            bool isRight;

            queryString = "select * from admin where adminUserName = " + SqlString.GetQuotedString(this.adminUserName);
            DataBase db = new DataBase();
            user = db.GetRecord(queryString);
            if (false == user)
            {
                errMessage = "抱歉不存在此用户名!";
                return false;
            }

            queryString = "select * from admin where adminUserName = " + SqlString.GetQuotedString(this.adminUserName);
            queryString = queryString + " and adminPassword = " + SqlString.GetQuotedString(this.adminPassword);
            isRight = db.GetRecord(queryString);
            if (false == isRight)
            {
                errMessage = "抱歉，用户和密码错误!";
                return false;
            }

            return true;
        }

        public bool ChangePassword()
        {
            string updateString = "update admin set adminPassword=" + SqlString.GetQuotedString(adminPassword);
            updateString += " where adminUserName=" + SqlString.GetQuotedString(adminUserName);

            DataBase db = new DataBase();
            if (db.InsertOrUpdate(updateString) < 0)
                return false;
            return true;
        }
    }
}

