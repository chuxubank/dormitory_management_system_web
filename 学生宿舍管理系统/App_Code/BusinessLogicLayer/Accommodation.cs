﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.DataAccessLayer;
using StudentApartment.DataAccessHelper;


namespace StudentApartment.BusinessLogicLayer
{
    public class Accommodation
    {

        private string studentNumber;
        private string buildingName; 
        private string roomNo; 
        private int accommodationinYear; 
        private int accommodationinMonth; 
        private int accommodationinDay;
        private string accommodationNotes; 
        private string errMessage;　

      
        #region MyRegion 
        public string StudentNumber
        {
            set
            {
                this.studentNumber = value;
            }
            get
            {
                return this.studentNumber;
            }
        }
        public string BuildingName
        {
            set
            {
                this.buildingName = value;
            }
            get
            {
                return this.buildingName;
            }
        }
        public string RoomNo
        {
            set
            {
                this.roomNo = value;
            }
            get
            {
                return roomNo;
            }
        }
        public int AccommodationinYear
        {
            set
            {
                this.accommodationinYear  = value;
            }
            get
            {
                return this.accommodationinYear ;
            }
        }
        public int AccommodationinMonth
        {
            set
            {
                this.accommodationinMonth  = value;
            }
            get
            {
                return this.accommodationinMonth;
            }
        }
        public int AccommodationinDay
        {
            set
            {
                this.accommodationinDay = value;
            }
            get
            {
                return this.accommodationinDay;
            }
        }
        public string AccommodationNotes
        {
            set
            {
                this.accommodationNotes = value;
            }
            get
            {
                return this.accommodationNotes;
            }
        }
        public string ErrMessage
        {
            set
            {
                this.errMessage = value;
            }
            get
            {
                return this.errMessage;
            }
        }
       
        #endregion
        
        public Accommodation()
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
        }

        public bool AddaccommodationInfo(string studentNumber, string buildingName, string roomNo, int accommodationInYear,
            int accommodationInMonth, int accommodationInDay, string accommodationNotes)
        {
            string queryString = "select * from studentInfo where studentNumber=" + SqlString.GetQuotedString(studentNumber);
            DataBase db = new DataBase();
            if (false == db.GetRecord(queryString))
            {
                this.ErrMessage = "学生信息不存在！";
                return false;
            }

            queryString = "select * from accommodation where studentNumber=" + SqlString.GetQuotedString(studentNumber);
            if (db.GetRecord(queryString))
            {
                this.ErrMessage = "抱歉，此学生已入住宿舍！";
                return false;
            }

            Room room = new Room();
            int leftNumberBed = room.GetLeftNumberOfBed(roomNo);
            if (leftNumberBed <= 0)
            {
                this.ErrMessage = "抱歉，您选择的房间已没有空余床位!";
                return false;
            }

            string insertString = "insert into accommodation(studentNumber,buildingName,roomNo,accommodationinYear,accommodationinMonth,accommodationinDay,accommodationNotes) values (";
            insertString += SqlString.GetQuotedString(studentNumber) + ",";
            insertString += SqlString.GetQuotedString(buildingName) + ",";
            insertString += SqlString.GetQuotedString(roomNo) + ",";
            insertString += accommodationInYear + ",";
            insertString += accommodationInMonth + ",";
            insertString += accommodationInDay + ",";
            insertString += SqlString.GetQuotedString(accommodationNotes) + ")";

            string updateString = "update roomInfo set leftNumberOfBed=leftNumberOfBed-1 where roomNo=" + SqlString.GetQuotedString(roomNo);

            string[] sqlstring = new string[] { insertString, updateString };
            if (!db.ExecuteSQL(sqlstring))
            {
                this.ErrMessage = "添加入住信息失败!";
                return false;
            }
            return true;
        }

        public string GetAccommodationTime(string studentNumber)
        {
            string queryString = "select accommodationinYear,accommodationinMonth,accommodationinDay from accommodation where studentNumber=" + SqlString.GetQuotedString(studentNumber);
            DataBase db = new DataBase();
            DataSet ds = db.GetDataSet(queryString);
            if (ds.Tables[0].Rows.Count == 0)
                return "";
            DataRow dr = ds.Tables[0].Rows[0];
            return dr[0].ToString() + "年" + dr[1].ToString() + "月" + dr[2].ToString() + "日";
        }

        public DataSet GetAccommodationInfo(string studentNumber, string studentName, string buildingName, string roomNo)
        {
            string queryString = "select * from accommodationView where studentNumber like '%" + studentNumber + "%'";
            queryString += " and studentName like '%" + studentName + "%'";
            queryString += " and buildingName like '%" + buildingName + "%'";
            queryString += " and roomNo like '%" + roomNo + "%'";
            DataSet ds = (new DataBase()).GetDataSet(queryString);
            return ds;
        }
   
        public bool CheckoutRoom(string studentNumber)
        {
            string queryString = "select roomNo from accommodation where studentNumber=" + SqlString.GetQuotedString(studentNumber);
            DataSet ds = (new DataBase()).GetDataSet(queryString);
            string roomNo = ds.Tables[0].Rows[0]["roomNo"].ToString();

            string deleteString = "delete from accommodation where studentNumber=" + SqlString.GetQuotedString(studentNumber);
            string updateString = "update roomInfo set leftNumberOfBed = leftNumberOfBed + 1 where roomNo =" + SqlString.GetQuotedString(roomNo);

            string[] sqlStrings = new string[]{deleteString,updateString};
            return (new DataBase()).ExecuteSQL(sqlStrings);
        }

        public bool QueryAccommodationInfo(string studentNumber)
        {
            string queryString = "select * from accommodationView where studentNumber=" + SqlString.GetQuotedString(studentNumber);
            DataBase db = new DataBase();
            DataSet ds = db.GetDataSet(queryString);
            if (ds.Tables[0].Rows.Count == 0)
            {
                this.errMessage = "没此学生的住宿信息";
                return false;
            }

            DataRow dr = ds.Tables[0].Rows[0];
            this.studentNumber = studentNumber;
            this.buildingName = dr["buildingName"].ToString();
            this.roomNo = dr["roomNo"].ToString();
            this.accommodationinYear = Convert.ToInt16(dr["accommodationinYear"].ToString());
            this.accommodationinMonth = Convert.ToInt16(dr["accommodationinMonth"].ToString());
            this.accommodationinDay = Convert.ToInt16(dr["accommodationinDay"].ToString());
            this.accommodationNotes = dr["accommodationNotes"].ToString();

            return true;
        }


        public bool UpdateAccommodationInfo()
        {
            Room room = new Room();
            int leftNumberOfBed = room.GetLeftNumberOfBed(this.roomNo);
            if (leftNumberOfBed <= 0)
            {
                this.errMessage = "抱歉，你选择的房间已无空余床位！";
                return false;
            }

            string oldRoomNo = this.GetOldRoomNo(); 
            string updateOldRoomString;
            string updateNewRoomString;
            string updateAccommodationString;
            updateOldRoomString = "update roomInfo set leftNumberOfBed = leftNumberOfBed + 1 where roomNo=" + SqlString.GetQuotedString(oldRoomNo);
            updateNewRoomString = "update roomInfo set leftNumberOfBed = leftNumberOfBed - 1 where roomNo=" + SqlString.GetQuotedString(this.roomNo);
            updateAccommodationString = "update accommodation set buildingName=" + SqlString.GetQuotedString(this.buildingName);
            updateAccommodationString += ",roomNo=" + SqlString.GetQuotedString(this.roomNo);
            updateAccommodationString += ",accommodationinYear=" + this.accommodationinYear;
            updateAccommodationString += ",accommodationinMonth=" + this.accommodationinMonth;
            updateAccommodationString += ",accommodationinDay=" + this.accommodationinDay;
            updateAccommodationString += ",accommodationNotes=" + SqlString.GetQuotedString(this.accommodationNotes);
            updateAccommodationString += " where studentNumber=" + SqlString.GetQuotedString(this.studentNumber);

            string[] updateSqls = new string[]{updateOldRoomString,updateNewRoomString,updateAccommodationString};
            DataBase db = new DataBase();
            if (!db.ExecuteSQL(updateSqls))
            {
                this.errMessage = "更换房间失败！";
                return false;
            }
            return true;

        }

        public string GetOldRoomNo()
        {
            string queryString = "select roomNo from accommodation where studentNumber=" + SqlString.GetQuotedString(this.studentNumber);
            DataBase db = new DataBase();
            DataSet ds = db.GetDataSet(queryString);
            if (ds.Tables[0].Rows.Count > 0)
                return ds.Tables[0].Rows[0]["roomNo"].ToString();
            return String.Empty;
        }

        public float GetRoomPrice(string studentNumber)
        {
            string queryString = "select roomPrice from roomInfo,accommodation where roomInfo.roomNo = accommodation.roomNo and accommodation.studentNumber=" + SqlString.GetQuotedString(studentNumber);
            DataSet ds = (new DataBase()).GetDataSet(queryString);
            if (ds.Tables[0].Rows.Count > 0)
                return Convert.ToSingle(ds.Tables[0].Rows[0]["roomPrice"]);
            return 0.0f;
        }
    }
}
