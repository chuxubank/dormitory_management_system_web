﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.DataAccessHelper;
using StudentApartment.DataAccessLayer;


namespace StudentApartment.BusinessLogicLayer
{
    public class Room
    {
        private string roomNo; 
        private string buildingName; 
        private string roomType;
        private float roomPrice; 
        private int numberOfBed; 
        private int leftNumberOfBed; 
        private string roomTelephone; 
        private string roomNotes; 
        private string errMessage; 

        #region MyRegion
        public string RoomNo
        {
            set
            {
                this.roomNo = value;
            }
            get
            {
                return this.roomNo;
            }
        }

        public string BuildingName
        {
            set
            {
                this.buildingName = value;
            }
            get
            {
                return this.buildingName;
            }
        }
        public string RoomType
        {
            set
            {
                this.roomType = value;
            }
            get
            {
                return this.roomType;
            }
        }
        public float RoomPrice
        {
            set
            {
                this.roomPrice = value;
            }
            get
            {
                return this.roomPrice;
            }
        }
        public int NumberOfBed
        {
            set
            {
                this.numberOfBed = value;
            }
            get
            {
                return this.numberOfBed;
            }
        }
        public int LeftNumberOfBed
        {
            set
            {
                this.leftNumberOfBed = value;
            }
            get
            {
                return this.leftNumberOfBed;
            }
        }
        public string RoomTelephone
        {
            set
            {
                this.roomTelephone = value;
            }
            get
            {
                return this.roomTelephone;
            }
        }
        public string RoomNotes
        {
            set
            {
                this.roomNotes = value;
            }
            get
            {
                return this.roomNotes;
            }
        }

        public string ErrMessage
        {
            set
            {
                this.errMessage = value;
            }
            get
            {
                return this.errMessage;
            }
        }

        #endregion
        
        public Room()
        {
 
        }

        public bool InsertRoomInfo()
        {
            if(this.IsExistRoomNo()) 
                return false; 
            if (this.leftNumberOfBed > this.numberOfBed)
            {
                this.errMessage = "空余床位不能大于总床位数!";
                return false;
            }

            string insertString = "insert into roomInfo(roomNo,buildingName,roomType,roomPrice,numberOfBed,leftNumberOfBed,roomTelephone,roomNotes) values (";
            insertString = insertString + SqlString.GetQuotedString(this.roomNo) + ",";
            insertString = insertString + SqlString.GetQuotedString(this.buildingName) + ",";
            insertString = insertString + SqlString.GetQuotedString(this.roomType) + ",";
            insertString = insertString + roomPrice + ",";
            insertString = insertString + this.numberOfBed + ",";
            insertString = insertString + this.leftNumberOfBed + ",";
            insertString = insertString + SqlString.GetQuotedString(this.roomTelephone) + ",";
            insertString = insertString + SqlString.GetQuotedString(this.roomNotes) + ")";


            DataBase db = new DataBase();
            if(db.InsertOrUpdate(insertString)< 0)
            {
                this.ErrMessage = "添加房间信息失败!";
                return false;
            }
            return true;
        }

        public bool IsExistRoomNo()
        {
            bool isExist = true;
            string queryString = "select * from roomInfo where roomNo=" + SqlString.GetQuotedString(this.roomNo);
            DataBase db = new DataBase();
            if (db.GetRecord(queryString)==false)
                isExist = false;
            else
                this.ErrMessage = "此房间编号已存在！";
            return isExist;  
        }

        public DataSet GetbuildingName()
        {
            string queryString = "select buildingName from buildingInfo";
            DataBase db = new DataBase();
            return db.GetDataSet(queryString);
        }

        public DataSet GetRoomType()
        {
            string queryString = "select roomTypeName from roomTypeInfo";
            DataBase db = new DataBase();
            return db.GetDataSet(queryString);
        }

        public DataSet GetAllRoom()
        {
            string queryString = "select * from roomInfo";
            return (new DataBase()).GetDataSet(queryString);
        }

        public DataSet QueryRoomInfo(string roomNo, string buildingName, string roomType)
        {
            string queryString = "select * from roomInfo where roomNo like '%" + roomNo + "%'";
            queryString = queryString + " and buildingName like '%" + buildingName + "%'";
            queryString = queryString + " and roomType like '%" + roomType + "%'";
            return (new DataBase()).GetDataSet(queryString);
        }

        public bool DeleteRooms(string roomNos)
        {
            string queryString = "select count(*) from accommodation where roomNo in (" + roomNos + ")";
            DataBase db = new DataBase();
            if (db.GetRecordCount(queryString) > 0)
            {
                this.errMessage = "要删除的房间有学生使用，不能进行删除!";
                return false;
            }

            string deleteString = "delete from roomInfo where roomNo in (" + roomNos + ")";
            if (db.InsertOrUpdate(deleteString) < 0)
            {
                this.errMessage = "删除房间失败!";
                return false;
            }

            return true;

        }

        public void GetRoomInfo(string roomNo)
        {
            DataBase db = new DataBase();
            string queryString = "select * from roomInfo where roomNo=" + SqlString.GetQuotedString(roomNo);
            DataSet ds = db.GetDataSet(queryString);
            this.roomNo = roomNo;
            this.buildingName = ds.Tables[0].Rows[0]["buildingName"].ToString();
            this.roomType = ds.Tables[0].Rows[0]["roomType"].ToString();
            this.roomPrice = Convert.ToSingle(ds.Tables[0].Rows[0]["roomPrice"].ToString());
            this.numberOfBed = Convert.ToInt16(ds.Tables[0].Rows[0]["numberOfBed"].ToString());
            this.leftNumberOfBed = Convert.ToInt16(ds.Tables[0].Rows[0]["leftNumberOfBed"].ToString());
            this.roomTelephone = ds.Tables[0].Rows[0]["roomTelephone"].ToString();
            this.roomNotes = ds.Tables[0].Rows[0]["roomNotes"].ToString();

        }

        public bool UpdateRoomInfo(string roomType, float roomPrice, string roomTelephone, string roomNotes)
        {
            string updateString = "update roomInfo set roomType=" + SqlString.GetQuotedString(roomType);
            updateString += ",roomPrice=" + roomPrice;
            updateString += ",roomTelephone=" + SqlString.GetQuotedString(roomTelephone);
            updateString += ",roomNotes=" + SqlString.GetQuotedString(roomNotes);
            updateString += " where roomNo=" + SqlString.GetQuotedString(this.roomNo);

            DataBase db = new DataBase();
            if (db.InsertOrUpdate(updateString) < 0)
            {
                this.errMessage = "更新房间信息失败!";
                return false;
            }

            return true;
        }

        public int GetLeftNumberOfBed(string roomNo)
        {
            string queryString = "select leftNumberOfBed from roomInfo where roomNo=" + SqlString.GetQuotedString(roomNo);
            DataBase db = new DataBase();
            DataSet ds = db.GetDataSet(queryString);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return Convert.ToInt16(ds.Tables[0].Rows[0]["leftNumberOfBed"]);
            }
            return 0;
        }
    }

}
