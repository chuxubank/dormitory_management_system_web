﻿using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using System.Collections;
using StudentApartment.DataAccessHelper;
using StudentApartment.DataAccessLayer;

namespace StudentApartment.BusinessLogicLayer

{
    public class Payment
    {
        private int paymentId;
        private string studentNumber;
        private string studentName;
        private string paymentType;
        private int paymentYear;
        private int paymentMonth;
        private int paymentDay;
        private float paymentMoney;
        private string errMessage;

        #region MyRegion
        public int PaymentId
        {
            set
            {
                this.paymentId = value;
            }
            get
            {
                return this.paymentId;
            }
        }
        public string StudentNumber
        {
            set
            {
                this.studentNumber = value;
            }
            get
            {
                return this.studentNumber;
            }
        }
        public string StudentName
        {
            set
            {
                this.studentName = value;
            }
            get
            {
                return this.studentName;
            }
        }
        public string PaymentType
        {
            set
            {
                this.paymentType = value;
            }
            get
            {
                return this.paymentType;
            }
        }
        public int PaymentYear
        {
            set
            {
                this.paymentYear = value;
            }
            get
            {
                return this.paymentYear;
            }
        }
        public int PaymentMonth
        {
            set
            {
                this.paymentMonth = value;
            }
            get
            {
                return this.paymentMonth;
            }
        }
        public int PaymentDay
        {
            set
            {
                this.paymentDay = value;
            }
            get
            {
                return this.paymentDay;
            }
        }
        public float GiveMoney
        {
            set
            {
                this.paymentMoney = value;
            }
            get
            {
                return this.paymentMoney;
            }
        }
        public string ErrMessage
        {
            set
            {
                this.errMessage = value;
            }
            get
            {
                return this.errMessage;
            }
        }

        #endregion
        public Payment()
        {
            //
            // TODO: 在此处添加构造函数逻辑
            //
        }

        public bool InsertPayment()
        {
            if (!IsExistStudent())
            {
                this.errMessage = "抱歉,此学号不存在!";
                return false;
            }

            string insertString = "insert into paymentInfo(studentNumber,paymentType,paymentYear,paymentMonth,paymentDay,paymentMoney) values (";
            insertString += SqlString.GetQuotedString(this.studentNumber) + ",";
            insertString += SqlString.GetQuotedString(this.paymentType) + ",";
            insertString += this.paymentYear + ",";
            insertString += this.paymentMonth + ",";
            insertString += this.paymentDay + ",";
            insertString += this.paymentMoney + ")";
            DataBase db = new DataBase();
            if (db.InsertOrUpdate(insertString) < 0)
            {
                this.errMessage = "抱歉,缴费信息添加失败!";
                return false;
            }
            return true;
        }

        public bool IsExistStudent()
        {   
            string queryString = "select * from studentInfo where studentNumber=" + SqlString.GetQuotedString(studentNumber);
            DataBase db = new DataBase();
            return db.GetRecord(queryString);
        }
        
        public string GetPaymentTime(int paymentId)
        {
            string queryString = "select paymentYear,paymentMonth,paymentDay from paymentInfo where paymentId=" + paymentId;
            DataBase db = new DataBase();
            DataSet ds = db.GetDataSet(queryString);
            if (ds.Tables[0].Rows.Count == 0) return String.Empty;

            DataRow dr = ds.Tables[0].Rows[0];
            return dr[0] + "年" + dr[1] + "月" + dr[2] + "日";
        }

        public bool DeletePaymentInfo(int paymentId)
        {
            string deleteString = "delete from paymentInfo where paymentId=" + paymentId;
            DataBase db = new DataBase();
            if (db.InsertOrUpdate(deleteString) < 0) return false;
            return true;
        }

        public DataSet QueryPaymentInfo(string studentNumber, string studentName, string paymentType)
        {
            string queryString = "select * from paymentInfoView where studentNumber like '%" + studentNumber + "%'";
            queryString += " and studentName like '%" + studentName + "%'";
            queryString += " and paymentType like '%" + paymentType + "%'";
            DataBase db = new DataBase();
            return db.GetDataSet(queryString);
        }
        
        public bool GetPaymentInfo(int paymentId)
        {
            string queryString = "select * from paymentInfoView where paymentId=" + paymentId;
            DataBase db = new DataBase();
            DataSet ds = db.GetDataSet(queryString);
            if (ds.Tables[0].Rows.Count == 0)
            {
                this.errMessage = "获得缴费信息失败！";
                return false;
            }
            DataRow dr = ds.Tables[0].Rows[0];
            paymentId = Convert.ToInt32(dr["paymentId"].ToString());
            studentNumber = dr["studentNumber"].ToString();
            studentName = dr["studentName"].ToString();
            paymentType = dr["paymentType"].ToString();
            paymentYear = Convert.ToInt16(dr["paymentYear"].ToString());
            paymentMonth = Convert.ToInt16(dr["paymentMonth"].ToString());
            paymentDay = Convert.ToInt16(dr["paymentDay"].ToString());
            paymentMoney = Convert.ToSingle(dr["paymentMoney"].ToString());

            return true;
        }

        public bool UpdatePaymentInfo()
        {
            string updateString = "update paymentInfo set studentNumber=" + SqlString.GetQuotedString(this.studentNumber);
            updateString += ",paymentType=" + SqlString.GetQuotedString(this.paymentType);
            updateString += ",paymentYear=" + this.paymentYear;
            updateString += ",paymentMonth=" + this.paymentMonth;
            updateString += ",paymentDay=" + this.paymentDay;
            updateString += ",paymentMoney=" + this.paymentMoney;
            updateString += " where paymentId=" + this.paymentId;

            Student student = new Student();
            if (!this.IsExistStudent())
            {
                this.errMessage = "该学号不存在!";
                return false;
            }
            DataBase db = new DataBase();
            if (db.InsertOrUpdate(updateString) < 0)
            {
                this.errMessage = "缴费信息更新失败!";
                return false;
            }
            return true;
        }
    }
}


