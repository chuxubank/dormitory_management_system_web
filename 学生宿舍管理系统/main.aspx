<%@ Page Language="C#" AutoEventWireup="true"  CodeFile="main.aspx.cs" Inherits="main" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>学生宿舍管理系统</title>
    <style type="text/css">
        
        body
        {
        	background-image:url('images/bbsinfo.jpg');
        	margin-top:0px;	
        }
        
            .style2
            {
                height: 400px;
                background-image:url('images/maintitle.gif');
                
                
            }
        </style>
        
</head>
<body background="images/091dm.jpg">
    <form id="form1" runat="server">
    <div>
<table WIDTH=770 BORDER=0 CELLPADDING=0 CELLSPACING=0 align="center" style="background-color:#ccffff;">
<tr>
<td COLSPAN=2>
    <asp:Menu ID="Menu1" Width ="770" runat="server" BackColor="#9933cc" 
        DynamicHorizontalOffset="2" Font-Names="Verdana" Font-Size="small"
        ForeColor="White" Orientation="Horizontal" StaticSubMenuIndent="10px">
        <StaticSelectedStyle BackColor="#1C5E55" />
        <StaticMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
        <DynamicHoverStyle BackColor="#666666" ForeColor="White" />
        <DynamicMenuStyle BackColor="#E3EAEB" />
        <DynamicSelectedStyle BackColor="#1C5E55" />
        <DynamicMenuItemStyle HorizontalPadding="5px" VerticalPadding="2px" />
        <StaticHoverStyle BackColor="#666666" ForeColor="White" />
        <Items>
            <asp:MenuItem Text="首页" Value="首页"></asp:MenuItem>
            <asp:MenuItem Text="管理宿舍房间" Value="管理宿舍房间">
                <asp:MenuItem NavigateUrl="~/roomInfoManage.aspx" Text="查询房间信息" Value="查询房间信息">
                </asp:MenuItem>
                <asp:MenuItem NavigateUrl="~/roomInfoAdd.aspx" Text="添加房间信息" Value="添加房间信息">
                </asp:MenuItem>
            </asp:MenuItem>
            <asp:MenuItem Text="管理住宿信息" Value="管理住宿信息">
                <asp:MenuItem NavigateUrl="~/AccommodationInfoManage.aspx" Text="查询宿舍信息" Value="查询宿舍信息">
                </asp:MenuItem>
                <asp:MenuItem NavigateUrl="~/AccommodationInfoAdd.aspx" Text="入住宿舍登记" Value="入住宿舍登记">
                </asp:MenuItem>
            </asp:MenuItem>
            <asp:MenuItem Text="管理缴费信息" Value="管理缴费信息">
                <asp:MenuItem NavigateUrl="~/PaymentInfoManage.aspx" Text="查询缴费信息" Value="查询缴费信息">
                </asp:MenuItem>
                <asp:MenuItem NavigateUrl="~/PaymentInfoAdd.aspx" Text="宿舍缴费登记" Value="宿舍缴费登记">
                </asp:MenuItem>
            </asp:MenuItem>
            <asp:MenuItem Text="管理学生信息" Value="管理学生信息">
                <asp:MenuItem NavigateUrl="~/studentInfoManage.aspx" Text="查询学生信息" 
                    Value="查询学生信息"></asp:MenuItem>
                <asp:MenuItem NavigateUrl="~/studentInfoAdd.aspx" Text="添加学生信息" Value="添加学生信息">
                </asp:MenuItem>
            </asp:MenuItem>
            <asp:MenuItem Text="系统设置" Value="系统设置">
                <asp:MenuItem NavigateUrl="~/ProfessionSet.aspx" Text="专业信息设置" Value="新建项">
                </asp:MenuItem>
                <asp:MenuItem NavigateUrl="~/Password.aspx" Text="修改密码" Value="修改密码">
                </asp:MenuItem>
            </asp:MenuItem>
            <asp:MenuItem NavigateUrl="~/login.aspx" Text="退出" Value="退出"></asp:MenuItem>
        </Items>
    </asp:Menu>
            
</td>
</tr>
	<tr style=" height:162px;">
		<td COLSPAN=2 style="height:162px;">
	        <img src="images/maintitle.jpg" style="width: 770px" />
	    </td>
	</tr>
	<tr>
		<td COLSPAN=2 style="background-color: #ccffff; font-size: 12px; 
		     height: 20px;">
            <table cellspacing="0" style=" margin-top:0px;">
                <tr>
                    <td style="width: 806px; height: 18px;">
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<img src="images/IcoUsername.gif" /> 操作员：<asp:Label ID="Label1" runat="server"
                Text="Label"></asp:Label></td>
                    <td align="right" style="width: 650px; height: 18px;">
                        <img src="images/icon1.gif" /> 登陆时间：<asp:Label ID="Label2" runat="server" Text="Label"></asp:Label>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                    </td>
                </tr>
            </table>
        </td>
	</tr>
	
	<tr>
<td bgcolor="#ccffff" style="height: 400px" class ="style2">&nbsp;</tr>
	
</table>
   		
    </div>
    </form>
  

</body>
</html>
