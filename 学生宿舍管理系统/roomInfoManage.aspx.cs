﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class roomInfoManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}
        if (!IsPostBack)
        {
            InitData();
        }
    }

    protected void BtnQuery_Click(object sender, EventArgs e)
    {
        string roomNo = this.RoomNo.Text;
        string buildingName = this.buildingName.SelectedValue;
        string roomType = this.roomType.SelectedValue;

        this.GridView1.DataSource = (new Room()).QueryRoomInfo(roomNo, buildingName, roomType);
        this.GridView1.DataBind();

    }

    public void InitData()
    {
        ListItem li = new ListItem("请选择所属楼名","");
        this.buildingName.Items.Add(li);
        DataSet ds = (new Room()).GetbuildingName();
        int count = ds.Tables[0].Rows.Count;
        string buildingName;
        for (int i = 0; i < count; i++)
        {
            buildingName = ds.Tables[0].Rows[i]["buildingName"].ToString();
            li = new ListItem(buildingName, buildingName);
            this.buildingName.Items.Add(li);
        }

        li = new ListItem("请选择房间类型", "");
        this.roomType.Items.Add(li);
        ds = (new Room()).GetRoomType();
        count = ds.Tables[0].Rows.Count;
        string roomTypeName;
        for (int i = 0; i < count; i++)
        {
            roomTypeName = ds.Tables[0].Rows[i]["roomTypeName"].ToString();
            li = new ListItem(roomTypeName, roomTypeName);
            this.roomType.Items.Add(li);
        }
        GridViewBind();
    }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "color=this.style.backgroundColor;this.style.backgroundColor='#ccffff';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=color;");
        }
    }

    public void GridViewBind()
    {
        Room room = new Room();
        DataSet ds = room.GetAllRoom();
        this.GridView1.DataSource = ds;
        this.GridView1.DataBind();
    }

    protected void BtnDel_Click(object sender, EventArgs e)
    {
        int Count = 0;　
        string roomNos = ""; 
        string RoomNo; 
        foreach (GridViewRow gr in GridView1.Rows)
        {
            CheckBox chk = (CheckBox)gr.Cells[0].FindControl("checkBox");
            if (chk.Checked) 
            {
                RoomNo = gr.Cells[1].Text;
                if (Count==0)
                    roomNos = "'" + RoomNo + "'";
                else
                    roomNos = roomNos + ",'" + RoomNo + "'";
                Count++;

            }
        }
        if (0 == Count) 
            Response.Write("<script>alert('抱歉，请选择房间!');</script>");
        else
        {
            Room room = new Room();
            bool result = room.DeleteRooms(roomNos);
            if (result==true )
                Response.Write("<script>alert('房间信息删除成功!')</script>");
            else
                Response.Write("<script>alert('" + room.ErrMessage + "');</script>");

        }
    }
    protected void CheckALL_CheckedChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < this.GridView1.Rows.Count; i++)
        {
            GridViewRow gr = this.GridView1.Rows[i];
            CheckBox chk = (CheckBox)gr.Cells[0].FindControl("checkBox");
            chk.Checked = this.CheckALL.Checked; 
        }
    }
}
