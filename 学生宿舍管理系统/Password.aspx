﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Password.aspx.cs" Inherits="Password" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>无标题页</title>
    <style type="text/css">
        .STYLE1 {color: #FF0000; font-family:微软雅黑}
        body,td,th {
	        font-size: 9pt;
	        border:solid 1px #dddddd;
        }
        body 
        {
	        margin:20px 0px 0px 0px;
	        background-image:url('images/bg.gif');
        }
        .style1
        {
            font-weight: bold;
            font-size: large;
            text-align: center;
            color: #FFFFFF; font-family:微软雅黑;
        }
        .style2
        {
            text-align:left;
        }
        .style3
        {
        	text-align:center;	
        }
    </style>
</head>
<body background="images/091dm.jpg">
     <form id="Form1" name="form1" method="post" runat="server">
     
     <div style="border-width:1px; margin:0px auto; width:92%; background-image:url(images/bg_li.gif); border-color:#D0E9FF; height:400px; padding-top:200px; border-width:3px; border-style:solid;">
         <table width="55%" border="1" cellpadding="0" cellspacing="2" align="center" style="height: 31px; border-collapse:collapse;">
             <tr bgcolor="#507CD1">
                 <td class="style1">
                     修改密码
                 </td>
             </tr>
         </table>
         <table width="55%" border="1" cellspacing="0" cellpadding="0" align="center">
             <tr>
                 <td>
                     <table width="100%" border="1" cellspacing="1" cellpadding="2" style="border-collapse:collapse; border:solid 1px #efefef;" align="center">
                         <tr>
                             <td class="style3">
                                 输入修改的密码
                             </td>
                             <td class="style2">
                                 <asp:TextBox ID="NewPassword" runat="server" TextMode="Password" Width="144px"></asp:TextBox>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="NewPassword"
                                     ErrorMessage="密码输入不能为空！"></asp:RequiredFieldValidator>&nbsp;&nbsp;
                             </td>
                         </tr>
                         <tr>
                             <td class="style3">
                                 确认修改的密码:
                             </td>
                             <td class="style2">
                                 <asp:TextBox ID="NewPasswordAgain" runat="server" TextMode="Password" Width="145px"></asp:TextBox>
                                 <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="NewPassword"
                                     ControlToValidate="NewPasswordAgain" ErrorMessage="两次密码输入不一致！"></asp:CompareValidator>
                             </td>
                         </tr>
                         <tr>
                             <td height="30" colspan="4" class="style3">
                                 &nbsp;<asp:Button ID="BtnChangePassword" runat="server" OnClick="BtnChangePassword_Click"
                                     Text="提交" />
                                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                 <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/main.aspx">返回首页</asp:HyperLink>
                                 <br />
                                 <asp:Literal ID="ErrMessage" runat="server"></asp:Literal>
                             </td>
                         </tr>
                     </table>
                 </td>
             </tr>
         </table>
    </div>
 </form>
</body>
</html>
