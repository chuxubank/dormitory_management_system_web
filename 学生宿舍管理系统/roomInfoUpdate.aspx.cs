﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class roomInfoUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string roomNo = Request.QueryString["roomNo"].ToString(); 
        if(!IsPostBack)
            InitDataView(roomNo);

    }
    protected void BtnUpdate_Click(object sender, EventArgs e)
    {

        string roomType = this.roomType.SelectedValue;
        float roomPrice = Convert.ToSingle(this.roomPrice.Text);
        string roomTelephone = this.roomTelephone.Text;
        string roomNotes = this.roomNotes.Text;
        Room room = new Room();
        room.RoomNo = Request.QueryString["roomNo"];

        if (room.UpdateRoomInfo(roomType, roomPrice, roomTelephone, roomNotes))
            Response.Write("<script>alert('房间信息更新成功！');location.href='roomInfoUpdate.aspx?roomNo=" + room.RoomNo + "';</script>");
        else
            Response.Write("<script>alert('" + room.ErrMessage + "');</script>");

    }
    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("roomInfoManage.aspx");
    }

    public void InitDataView(string roomNo)
    {
        Room room = new Room();
        room.GetRoomInfo(roomNo);
        this.roomNo.Text = room.RoomNo;
        this.buildingName.Text = room.BuildingName;
        this.roomType.Text = room.RoomType;
        this.roomPrice.Text = room.RoomPrice.ToString();
        this.numberOfBed.Text = room.NumberOfBed.ToString();
        this.leftNumberOfBed.Text = room.LeftNumberOfBed.ToString();
        this.roomTelephone.Text = room.RoomTelephone;
        this.roomNotes.Text = room.RoomNotes;
    }
}
