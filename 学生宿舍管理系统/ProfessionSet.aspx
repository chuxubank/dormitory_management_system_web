﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ProfessionSet.aspx.cs" Inherits="ProfessionSet" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        <title></title>
        <style type="text/css">
            .STYLE1 {
                color: #FF0000
            }

            body,
            td,
            th {
                font-size: 9pt;
                text-align: center;
            }

            body {
                margin: 20px 0px 0px 0px;
                background-image: url('images/bg.jpg');
            }

            .style7 {
                height: 20px;
                font-size: large;
                font-weight: bold;
                text-align: center;
                color: #FFFFFF;
                font-family: 微软雅黑;
            }

            .style8 {
                text-align: center;
            }
        </style>
    </head>

    <body>
        <form id="form1" runat="server">
            <div style="border-width: 1px; margin: 0px auto; width: 92%; background-image: url(images/bg_li.gif);
        border-color: #D0E9FF; height: 530px; padding-top: 70px; border-width: 3px; border-style: solid;">
                <table align="center" width="100%" cellpadding="0" cellspacing="0">
                    <tr bgcolor="#507CD1">
                        <td class="style7">
                            设置专业信息
                        </td>
                    </tr>
                    <tr>
                        <td>
                            专业名称:&nbsp;&nbsp;
                            <asp:TextBox ID="professionName" runat="server" Width="100px"></asp:TextBox>
                            &nbsp;所属学院:
                            <asp:DropDownList ID="CollegeName" runat="server" DataSourceID="collegeNameDataSource"
                                DataTextField="collegeName" DataValueField="collegeName" Width="105px">
                            </asp:DropDownList>
                            &nbsp;
                            <asp:Button ID="BtnAdd" runat="server" OnClick="BtnAdd_Click" Text="添加" Width="35px" /> &nbsp;&nbsp;
                        </td>
                    </tr>
                    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC"
                        BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="professionInfoDataSource" OnRowDataBound="GridView1_RowDataBound"
                        Width="100%">
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <Columns>
                            <asp:TemplateField HeaderText="">
                                <ItemTemplate>
                                    <asp:CheckBox ID="checkBox" runat="server" />
                                </ItemTemplate>
                                <ControlStyle Width="20px" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="professionName" HeaderText="专业名称" SortExpression="professionName" />
                            <asp:BoundField DataField="collegeName" HeaderText="所属学院" SortExpression="collegeName" />
                        </Columns>
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <PagerStyle ForeColor="#000066" HorizontalAlign="Left" BackColor="White" />
                        <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    </asp:GridView>
                    <tr>
                        <td width="100%" class="style8">
                            <br />
                            <asp:CheckBox ID="checkALL" runat="server" AutoPostBack="True" OnCheckedChanged="checkALL_CheckedChanged" Text=" 全选" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="BtnDelete" runat="server" OnClick="BtnDelete_Click" Text="删除" Width="41px" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/main.aspx">返回首页</asp:HyperLink>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="professionInfoDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [professionName], [collegeName] FROM [professionInfo]">
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="collegeNameDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [collegeName] FROM [collegeInfo]">
                </asp:SqlDataSource>
            </div>
        </form>
    </body>

    </html>