<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentInfoUpdate.aspx.cs" Inherits="PaymentInfoUpdate" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
<Title></Title>
<style type="text/css">
.STYLE1 {color: #FF0000}
body,td,th {
	font-size: 9pt;
}
body {
	 margin:20px 0px 0px 0px;
	 background-image:url('images/bg.gif');
}
        .style5
    {
        height: 29px;
        font-size: large;
        font-weight: bold;
        text-align: center;
        color: #FFFFFF;
        font-family:微软雅黑;
    }
    </style>
</head>
   <BODY background="images/091dm.jpg">
 <form id="Form1" runat="server" method="post" name=form1>
 <div style="border-width: 1px; margin: 0px auto; width: 92%; background-image: url(images/bg_li.gif);
     border-color: #D0E9FF; height: 530px; padding-top: 70px; border-width: 3px; border-style: solid;">
     <table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
         <tr bgcolor="#507CD1">
             <td colspan="2" class="style5">
                 更新缴费信息
             </td>
         </tr>
         <tr>
             <td width="100" align="right">
                 <br />
                 学生学号:
             </td>
             <td>
                 <br />
                 <asp:TextBox ID="StudentNumber" runat="server" Width="80px"></asp:TextBox><font color="red">*<asp:RequiredFieldValidator
                     ID="RequiredFieldValidator1" runat="server" ControlToValidate="StudentNumber"
                     ErrorMessage="学生学号必填！" ValidationGroup="BtnUpdate"></asp:RequiredFieldValidator></font>
             </td>
         </tr>
         <tr>
             <td width="100" align="right">
                 <br />
                 学生姓名:
             </td>
             <td>
                 &nbsp;<br />
                 <asp:Literal ID="StudentName" runat="server"></asp:Literal>
             </td>
         </tr>
         <tr>
             <td width="100" align="right" style="height: 22px">
                 <br />
                 缴费类别:
             </td>
             <td style="height: 22px">
                 <br />
                 <asp:DropDownList ID="PaymentType" runat="server" Height="16px" Width="80px">
                     <asp:ListItem>住宿费</asp:ListItem>
                     <asp:ListItem>水电费</asp:ListItem>
                 </asp:DropDownList>
             </td>
         </tr>
         <tr>
             <td width="100" align="right" style="height: 18px">
                 <br />
                 缴费日期:
             </td>
             <td style="height: 18px">
                 <br />
                 <asp:DropDownList ID="PaymentYear" runat="server" Width="61px">
                 </asp:DropDownList>
                 年
                 <asp:DropDownList ID="PaymentMonth" runat="server" Width="38px">
                 </asp:DropDownList>
                 月<asp:DropDownList ID="PaymentDay" runat="server" Width="43px">
                 </asp:DropDownList>
                 日
             </td>
         </tr>
         <tr>
             <td width="100" align="right" style="height: 24px">
                 <br />
                 缴费金额:
             </td>
             <td style="height: 24px">
                 <br />
                 <asp:TextBox ID="Price" runat="server" Width="80px"></asp:TextBox>元<asp:CompareValidator
                     ID="CompareValidator1" runat="server" ErrorMessage="金额格式不正确！" ControlToValidate="Price"
                     Operator="DataTypeCheck" Type="Currency" ValidationGroup="BtnUpdate"></asp:CompareValidator>
             </td>
         </tr>
         <tr>
             <td colspan="2" align="center" style="height: 25px">
                 &nbsp;<br />
                 &nbsp;<asp:Button ID="BtnUpdate" runat="server" OnClick="BtnUpdate_Click" Text="提交" />
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:Button ID="BtnCancle" runat="server" OnClick="BtnCancle_Click" Text="取消" /><br />
                 <br />
                 <asp:Literal ID="ErrMessage" runat="server"></asp:Literal>
             </td>
         </tr>
     </table>
 </div>
</form>
</body>
</html>
