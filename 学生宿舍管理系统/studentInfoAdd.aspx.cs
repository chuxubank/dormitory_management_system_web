﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class studentInfoAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}

    }
    protected void BtnAdd_Click(object sender, EventArgs e)
    {
        String StudentNumber = this.StudentNumber.Text; 
        String StudentName = this.StudentName.Text; 
        String StudentSex = this.StudentSex.Text; 
        String StudentState = this.StudentState.Text; 
        String CollegeName = this.CollegeName.Text; 
        String StudentProfession = this.StudentProfession.Text; 
        DateTime StudentBirthday = Convert.ToDateTime(this.StudentBirthday.Text);
        String StudentAddress = this.StudentAddress.Text; 
        String StudentNotes = this.StudentNotes.Text; 
        Student student = new Student(StudentNumber,StudentName,StudentSex,StudentState,CollegeName,
           StudentProfession,StudentBirthday,StudentAddress,StudentNotes);
        student.InsertStudent(); 
        this.ErrMessage.Text = student.ErrMessage; 


    }
    protected void BtnCancle_Click(object sender, EventArgs e)
    {
        this.StudentNumber.Text = "";
        this.StudentName.Text = "";
        this.StudentSex.SelectedIndex = 0;
        this.StudentState.SelectedIndex = 0;
        this.CollegeName.SelectedIndex = 0;
        this.StudentProfession.SelectedIndex = 0;
        this.StudentBirthday.Text = "";
        this.StudentAddress.Text = "";
        this.StudentNotes.Text = "";
        this.ErrMessage.Text = "";
    }
}
