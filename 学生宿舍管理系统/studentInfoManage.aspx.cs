﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;
public partial class studentInfoManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}
        if (!IsPostBack)
        {
            InitData(); 
        }
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "color=this.style.backgroundColor;this.style.backgroundColor='#ccffff';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=color;");
            e.Row.Cells[6].Text = Convert.ToDateTime(e.Row.Cells[6].Text).ToShortDateString();
        }
    }
    protected void checkALL_CheckedChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < this.GridView1.Rows.Count; i++)
        {
            GridViewRow gr = this.GridView1.Rows[i];
            CheckBox chk = (CheckBox)gr.Cells[0].FindControl("checkBox");
            chk.Checked = this.checkALL.Checked; 
        }
    }
    protected void Button1Click(object sender, EventArgs e)
    {
        int Count = 0;　
        string studentNumbers = ""; 
        string StudentNumber; 
        foreach(GridViewRow gr in GridView1.Rows) 
        {
            CheckBox chk = (CheckBox)gr.Cells[0].FindControl("checkBox");
            if(chk.Checked) 
            {
                StudentNumber = gr.Cells[1].Text;
                if(0 == Count)
                    studentNumbers = "'" + StudentNumber + "'";
                else
                    studentNumbers = studentNumbers + ",'" + StudentNumber + "'";
                Count++;

            }
        }
        if(0 == Count) 
            Response.Write("<script>alert('抱歉，请选择学生!');</script>");
        else
        {
            Student student = new Student();
            student.DeleteStudents(studentNumbers); 
            Response.Write("<script>alert('" + student.ErrMessage + "')</script>");
        }
    }
    protected void BtnQuery_Click(object sender, EventArgs e)
    {
        string studentNumber = this.StudentNumber.Text;
        string studentName = this.StudentName.Text;
        string studentProfession = this.StudentProfession.SelectedValue;

        Student student = new Student();
        DataSet ds = student.QueryStudent(studentNumber,studentName,studentProfession); //根据条件查询学生信息

        this.GridView1.DataSource = ds;
        this.GridView1.DataBind();
    }

    public void InitData()
    {
        string professionName;
        ListItem li = new ListItem("请选择专业", "");
        this.StudentProfession.Items.Add(li);

        DataSet ds = (new Student()).QueryProfessionInfo();
        int rowCount = ds.Tables[0].Rows.Count;
        for(int i=0;i<rowCount;i++)
        {
            professionName = ds.Tables[0].Rows[i]["professionName"].ToString();
            li = new ListItem(professionName,professionName);
            this.StudentProfession.Items.Add(li);
        }
        GridViewBind();

    }

    public void GridViewBind()
    {
        Student student = new Student();
        DataSet ds = student.QueryAllStudent();
        this.GridView1.DataSource = ds;
        this.GridView1.DataBind();
    }
}
