﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PaymentInfoManage.aspx.cs" Inherits="PaymentInfoManage" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        <Title></Title>
        <style type="text/css">
            .STYLE1 {
                color: #FF0000
            }

            body,
            td,
            th {
                font-size: 9pt;
            }

            body {
                margin: 20px 0px 0px 0px;
                background-image: url('images/bg.gif');
            }

            .style5 {
                height: 23px;
                font-size: large;
                font-weight: bold;
                text-align: center;
                color: #FFFFFF;
                font-family: 微软雅黑;
            }

            .style6 {
                text-align: center;
            }
        </style>
    </head>

    <body>
        <form id="form1" runat="server">
            <div style="border-width: 1px; margin: 0px auto; width: 92%; background-image: url(images/bg_li.gif);
        border-color: #D0E9FF; height: 530px; padding-top: 70px; border-width: 3px; border-style: solid;">
                <table width="100%" border='1' align="center" cellpadding='0' cellspacing='0' bordercolor="#F0F0F0" id="TabDocMain">
                    <tr bgcolor="#507CD1">
                        <td class="style5">
                            查询缴费信息
                        </td>
                    </tr>
                    <tr>
                        <td colspan="7" class="style6">
                            <div class="style6">
                                学生学号:
                                <asp:TextBox ID="StudentNumber" runat="server" Width="91px"></asp:TextBox>
                                学生姓名:
                                <asp:TextBox ID="StudentName" runat="server" Width="84px"></asp:TextBox>
                                缴费类别:
                                <asp:DropDownList ID="PaymentType" runat="server">
                                    <asp:ListItem Selected="True">请选择类型</asp:ListItem>
                                    <asp:ListItem>住宿费</asp:ListItem>
                                    <asp:ListItem>水电费</asp:ListItem>
                                </asp:DropDownList>
                                <asp:Button ID="Button1" runat="server" Text="查询" OnClick="Button1_Click" />&nbsp;
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/main.aspx">返回首页</asp:HyperLink>
                                <br />
                            </div>
                            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC"
                                BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="paymentInfoDataSource" DataKeyNames="paymentId"
                                OnRowDataBound="GridView1_RowDataBound" Width="100%">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="checkBox" runat="server" />
                                        </ItemTemplate>
                                        <ControlStyle Width="30px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="paymentId" HeaderText="paymentId" SortExpression="paymentId" Visible="False" />
                                    <asp:BoundField DataField="studentNumber" HeaderText="学生学号" SortExpression="studentNumber" />
                                    <asp:BoundField DataField="studentName" HeaderText="学生姓名" SortExpression="studentName" />
                                    <asp:BoundField DataField="paymentType" HeaderText="缴费类别" SortExpression="paymentType" />
                                    <asp:BoundField DataField="paymentMoney" HeaderText="缴费金额" SortExpression="paymentMoney" />
                                    <asp:TemplateField HeaderText="缴费日期">
                                        <ItemTemplate>
                                            <asp:Literal ID="paymentMoneyTime" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:HyperLinkField DataNavigateUrlFields="paymentId" DataNavigateUrlFormatString="paymentInfoUpdate.aspx?paymentId={0}"
                                        HeaderText="操作" Text="更新" />
                                </Columns>
                                <RowStyle ForeColor="#000066" />
                                <EmptyDataTemplate>
                                    抱歉，您要查询的信息不存在!
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <PagerStyle ForeColor="#000066" HorizontalAlign="Left" BackColor="White" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                            <asp:CheckBox ID="checkALL" runat="server" AutoPostBack="True" OnCheckedChanged="checkALL_CheckedChanged" Text=" 全选" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Btn_Deleter" runat="server" OnClick="BtnDelete_Click" Text="删除" />
                            <asp:SqlDataSource ID="paymentInfoDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.SqlClient" SelectCommand="SELECT [paymentId], [studentNumber], [paymentType], [paymentYear], [paymentMonth], [paymentDay], [paymentMoney], [studentName] FROM [paymentInfoView]">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <br />
            </div>
        </form>
    </body>

    </html>