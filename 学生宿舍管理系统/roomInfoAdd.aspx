<%@ Page Language="C#" AutoEventWireup="true" CodeFile="roomInfoAdd.aspx.cs" Inherits="roomInfoAdd" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <Title></Title>
        <META http-equiv="Content-Type" content="text/html; charset=gb2312" />
        <style type="text/css">
            .STYLE1 {
                color: #FF0000
            }

            body,
            td,
            th {
                font-size: 9pt;
            }

            body {
                margin: 20px 0px 0px 0px;
                background-image: url('images/bg.gif');
            }

            .style5 {
                font-size: large;
                font-weight: bold;
                color: #FFFFFF;
                font-family: 微软雅黑;
                text-align: center;
                height: 24px;
            }

            .style6 {
                font-size: small;
            }
        </style>
    </head>

    <body>
        <div style="border-width:1px; margin:0px auto; width:92%; background-image:url(images/bg_li.gif); border-color:#D0E9FF; height:530px; padding-top:70px; border-width:3px; border-style:solid;">
            <table width="95%" align="center" cellpadding='0' cellspacing='0' style="border:solid 1px #0066cc;" id="TabDocMain">
                <table width=600 border=0 cellpadding=0 cellspacing=0 align="center">
                    <form action="" method="post" name=form1 runat=server id="Form1">
                        <tr bgcolor="#507CD1">
                            <td colspan="2" class="style5">添加房间信息
                                <font>
                                    <span class="style6">(带*号的为必填项)</span>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td width=100 align="right" style="height: 24px">房间编号:</td>
                            <td style="height: 24px">&nbsp;&nbsp;
                                <asp:TextBox ID="roomNo" runat="server" Width="96px"></asp:TextBox>
                                <font>
                                    <span class="style6">*(格式为"楼名-房间编号")</span>
                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="roomNo" ErrorMessage="房间编号必填！"></asp:RequiredFieldValidator>
                                </font>
                            </td>
                        </tr>
                        <tr>
                            <td width=100 align="right">所属楼名:</td>
                            <td>&nbsp;
                                <asp:DropDownList ID="buildingName" runat="server" DataSourceID="apartmentInfoDataSource" DataTextField="buildingName" DataValueField="buildingName">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="apartmentInfoDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [buildingName] FROM [buildingInfo]">
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td width=100 align="right">房间类别:</td>
                            <td>&nbsp;&nbsp;
                                <asp:DropDownList ID="roomType" runat="server" DataSourceID="roomTypeDataSource" DataTextField="roomTypeName" DataValueField="roomTypeName">
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="roomTypeDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="System.Data.SqlClient"
                                    SelectCommand="SELECT [roomTypeName] FROM [roomTypeInfo]">
                                </asp:SqlDataSource>
                            </td>
                        </tr>
                        <tr>
                            <td width=100 align="right" style="height: 24px">房间价格:</td>
                            <td style="height: 24px">&nbsp;&nbsp;
                                <asp:TextBox ID="roomPrice" runat="server" Width="80px"></asp:TextBox>元/月&nbsp;
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="roomPrice" ErrorMessage="房间价格必填！"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="roomPrice" ErrorMessage="输入的价格不正确！" Operator="DataTypeCheck"
                                    Type="Currency"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width=100 align="right">总床位数:</td>
                            <td>&nbsp;&nbsp;
                                <asp:TextBox ID="numberOfBed" runat="server" Width="79px"></asp:TextBox>个
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="numberOfBed" ErrorMessage="总床位数必填！"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="numberOfBed" ErrorMessage="输入的数字不正确！" Operator="DataTypeCheck"
                                    Type="Integer"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width=100 align="right" style="height: 24px">现余床位:</td>
                            <td style="height: 24px">&nbsp;&nbsp;
                                <asp:TextBox ID="leftNumberOfBed" runat="server" Width="78px"></asp:TextBox>个
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ControlToValidate="leftNumberOfBed" ErrorMessage="现余床位必填！"></asp:RequiredFieldValidator>
                                <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="leftNumberOfBed" ErrorMessage="输入的数字不正确！"
                                    Operator="DataTypeCheck" Type="Integer"></asp:CompareValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width=100 align="right" style="height: 24px">宿舍电话:</td>
                            <td style="height: 24px">&nbsp;&nbsp;
                                <asp:TextBox ID="roomTelephone" runat="server" Width="146px"></asp:TextBox>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="roomTelephone" ErrorMessage="电话格式不正确!"
                                    ValidationExpression="^[0-9]{3,4}-[0-9]{7,8}" Width="116px"></asp:RegularExpressionValidator>
                            </td>
                        </tr>
                        <tr>
                            <td width=100 align="right">备&nbsp;&nbsp;&nbsp; 注:</td>
                            <td>&nbsp;&nbsp;
                                <asp:TextBox ID="roomNotes" runat="server" Height="49px" Width="356px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan=2 align="center" style="height: 22px">
                                &nbsp;&nbsp;
                                <asp:Literal ID="ErrMessage" runat="server"></asp:Literal>
                                <br />
                                <asp:Button ID="BtnAdd" runat="server" Text="添加" OnClick="BtnAdd_Click" />&nbsp;&nbsp
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/main.aspx">返回首页</asp:HyperLink>
                            </td>
                        </tr>
                    </form>
                </table>
            </table>
        </div>
    </body>

    </html>