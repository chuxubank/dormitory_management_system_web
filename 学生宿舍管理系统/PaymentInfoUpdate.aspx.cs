﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;

using StudentApartment.BusinessLogicLayer;

public partial class PaymentInfoUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}

        if (!IsPostBack)
        {
            int i;
            for (i = 2000; i <= 2030; i++)
                this.PaymentYear.Items.Add(new ListItem(i.ToString(),i.ToString()));
            for (i = 1; i <= 12; i++)
                this.PaymentMonth.Items.Add(new ListItem(i.ToString(),i.ToString()));
            for (i = 1; i <= 31; i++)
                this.PaymentDay.Items.Add(new ListItem(i.ToString(),i.ToString()));

            string paymentId = Request.QueryString["paymentId"];
            Payment money = new Payment();
            money.GetPaymentInfo(Convert.ToInt32(paymentId));
            this.StudentNumber.Text = money.StudentNumber;
            this.StudentName.Text = money.StudentName;
            this.PaymentType.SelectedValue = money.PaymentType;
            this.PaymentYear.SelectedValue = money.PaymentYear.ToString();
            this.PaymentMonth.SelectedValue = money.PaymentMonth.ToString();
            this.PaymentDay.SelectedValue = money.PaymentDay.ToString();
            this.Price.Text = money.GiveMoney.ToString();
        }

      

    }
    protected void BtnUpdate_Click(object sender, EventArgs e)
    {
        Payment money = new Payment();
        money.PaymentId = Convert.ToInt32(Request.QueryString["paymentId"]);
        money.StudentNumber = this.StudentNumber.Text;
        money.PaymentType = this.PaymentType.SelectedValue;
        money.PaymentYear = Convert.ToInt16(this.PaymentYear.SelectedValue);
        money.PaymentMonth = Convert.ToInt16(this.PaymentMonth.SelectedValue);
        money.PaymentDay = Convert.ToInt16(this.PaymentDay.SelectedValue);
        money.GiveMoney = Convert.ToSingle(this.Price.Text);
        if (money.UpdatePaymentInfo())
            Response.Write("<script>alert('缴费信息更新成功!');location.href='PaymentInfoManage.aspx';</script>");
        else
            Response.Write("<script>alert('" + money.ErrMessage + "');location.href='PaymentInfoUpdate.aspx?paymentId=" + Request.QueryString["paymentId"] + "';</script>");
    }
    protected void BtnCancle_Click(object sender, EventArgs e)
    {
        Response.Redirect("PaymentInfoManage.aspx");
    }
}
