﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccommodationInfoAdd.aspx.cs" Inherits="AccommodationInfoAdd" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <title></title>

        <head id="Head1" runat="server">
            <title></title>
            <style type="text/css">
                .STYLE1 {
                    color: #FF0000
                }

                body,
                td,
                th {
                    font-size: 9pt;
                }

                body {
                    margin: 20px 0px 0px 0px;
                    background-image: url('images/bg.gif');
                }

                .style5 {
                    font-size: large;
                    text-align: center;
                    height: 21px;
                    font-family: 微软雅黑;
                }

                .style6 {
                    color: #FFFFFF;
                    font-weight: bold;
                }

                .style7 {
                    height: 27px;
                }
            </style>
        </head>

        <body>
            <form id="Form1" action="" method="post" name=form1 runat=server>
                <div style="border-width:1px; margin:0px auto; width:92%; background-image:url(images/bg_li.gif); border-color:#D0E9FF; height:530px; padding-top:70px; border-width:3px; border-style:solid;">

                    <table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
                        <tr bgcolor="#507CD1">
                            <td colspan="2" class="style5">
                                <span class="style6">入住宿舍登记</span>
                                </b>
                            </td>
                        </tr>
                        <tr>
                            <td width="100" align="right" class="style7">
                                <br /> 学生编号:
                            </td>
                            <td class="style7">
                                <br />
                                <asp:TextBox ID="studnetNumber" runat="server" Width="89px">
                                </asp:TextBox>
                                <asp:RequiredFieldValidator runat="server" ErrorMessage="学生编号必填" ControlToValidate="studnetNumber" Display="Dynamic" ValidationGroup="BtnAdd">
                                </asp:RequiredFieldValidator>
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="100" align="right" class="style7">
                                所在楼名:
                            </td>
                            <td class="style7">
                                <br />
                                <asp:DropDownList ID="buildingName" runat="server" Width="89px" AutoPostBack="True" DataSourceID="buildingNameDataSource"
                                    DataTextField="buildingName" DataValueField="buildingName">
                                </asp:DropDownList>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="100" align="right" style="height: 18px">
                                房间编号:
                            </td>
                            <td style="height: 18px">
                                <asp:DropDownList ID="roomNo" runat="server" AutoPostBack="True" DataSourceID="RoomNoDataSource" DataTextField="roomNo" DataValueField="roomNo">
                                </asp:DropDownList>&nbsp;
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td width="100" align="right">
                                入住时间:
                            </td>
                            <td>
                                <asp:DropDownList ID="Year" runat="server" Width="58px">
                                </asp:DropDownList>年
                                <asp:DropDownList ID="Month" runat="server" Width="38px">
                                </asp:DropDownList>月
                                <asp:DropDownList ID="Day" runat="server" Width="41px">
                                </asp:DropDownList>
                                日
                                <br />
                                <br />
                        </tr>
                        <tr>
                            <td width="100" align="right" style="height: 62px">
                                备 &nbsp;&nbsp; 注:
                            </td>
                            <td style="height: 62px">
                                <asp:TextBox ID="accommodationNotes" runat="server" Height="37px" TextMode="MultiLine" Width="301px">
                                </asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center" style="height: 22px">
                                <asp:Button ID="BtnAdd" runat="server" OnClick="BtnAdd_Click" Text="添加" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:Button ID="BtnBack" runat="server" OnClick="BtnBack_Click" Text="返回" />
                            </td>
                        </tr>
                    </table>
                    <asp:SqlDataSource ID="buildingNameDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                        ProviderName="System.Data.SqlClient" SelectCommand="SELECT [buildingName] FROM [buildingInfo]">
                    </asp:SqlDataSource>
                    <br />
                    <asp:SqlDataSource ID="RoomNoDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="System.Data.SqlClient"
                        SelectCommand="SELECT [roomNo] FROM [roomInfo] WHERE ([buildingName] = @buildingName)">
                        <selectparameters>
                            <asp:ControlParameter ControlID="buildingName" Name="buildingName" PropertyName="SelectedValue" Type="String" />
                        </selectparameters>
                    </asp:SqlDataSource>
                </div>
            </form>
        </body>

    </HTML>