<%@ Page Language="C#" AutoEventWireup="true" CodeFile="studentInfoAdd.aspx.cs" Inherits="studentInfoAdd" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
	<Title></Title>
	<META http-equiv="Content-Type" content="text/html; charset=gb2312" />
<style type="text/css">
.STYLE1 {color: #FF0000}
body,td,th {
	font-size: 9pt;
	border:solid 1px #6699cc;
}
body {
	 margin:20px 0px 0px 0px;
	 background-image:url('images/bg.gif');
}
        .style5
    {
        height: 24px;
        text-align: center;
       
    }
    .style6
    {
        font-size: large;
        font-weight: bold;
        color: #FFFFFF;
        font-family:微软雅黑;
    }
    .style7
    {
        color: #FFFFFF;
    }
    .style8
    {
        height: 32px;
        width: 132px;
    }
    .style9
    {
        height: 29px;
        width: 132px;
    }
    .style10
    {
        height: 58px;
        width: 132px;
    }
    .style11
    {
        height: 29px;
    }
    .style12
    {
        height: 28px;
    }
    </style>
</head>
<body>
 <form  method="post" name=form1 runat = "server">
 
 <div style="border-width: 1px; margin: 0px auto; width: 92%; background-image: url(images/bg_li.gif);
     border-color: #D0E9FF; height: 530px; padding-top: 70px; border-width: 3px; border-style: solid;">
     &nbsp;<table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
         <tr bgcolor="#507CD1">
             <td colspan="2" class="style5">
                 <span><span class="style6">学生信息添加</span> <span class="style7"><font size="-1">(带*号的为必填项)</span></span>
             </td>
         </tr>
         <tr>
             <td width="25%" class="style12">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;学生学号:
             </td>
             <td class="style12">
                 <asp:TextBox ID="StudentNumber" runat="server" Width="107px" Style="margin-left: 0px"></asp:TextBox><font
                     color="red"> *
                     <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="StudentNumber"
                         ErrorMessage="学生学号必填！" Width="109px"></asp:RequiredFieldValidator>
                 </font>
             </td>
         </tr>
         <tr>
             <td class="style8">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;学生姓名:
             </td>
             <td>
                 <asp:TextBox ID="StudentName" runat="server" Width="107px"></asp:TextBox><font color="red">
                     *<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="StudentName"
                         ErrorMessage="学生姓名必填" Width="237px"></asp:RequiredFieldValidator></font>
             </td>
         </tr>
         <tr>
             <td class="style9">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;学生性别:
             </td>
             <td class="style11">
                 <asp:DropDownList ID="StudentSex" runat="server" Height="22px" Width="106px">
                     <asp:ListItem>男</asp:ListItem>
                     <asp:ListItem>女</asp:ListItem>
                 </asp:DropDownList>
             </td>
         </tr>
         <tr>
             <td class="style8">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;政治面貌:
             </td>
             <td style="height: 21px">
                 <asp:DropDownList ID="StudentState" runat="server" Height="19px" Width="107px">
                     <asp:ListItem>团员</asp:ListItem>
                     <asp:ListItem>党员</asp:ListItem>
                     <asp:ListItem>群众</asp:ListItem>
                 </asp:DropDownList>
             </td>
         </tr>
         <tr>
             <td class="style8">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;所在学院:
             </td>
             <td>
                 <asp:DropDownList ID="CollegeName" runat="server" DataSourceID="CollegeDataSource"
                     DataTextField="collegeName" DataValueField="collegeName" AutoPostBack="True"
                     Height="16px" Width="107px">
                 </asp:DropDownList>
                 &nbsp;
             </td>
         </tr>
         <tr>
             <td class="style8">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;所学专业:
             </td>
             <td>
                 <asp:DropDownList ID="StudentProfession" runat="server" DataSourceID="StudentProfessionDataSource"
                     DataTextField="professionName" DataValueField="professionName" Height="16px"
                     Width="107px">
                 </asp:DropDownList>
             </td>
         </tr>
         <tr>
             <td class="style8">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;出生日期:
             </td>
             <td>
                 <asp:TextBox ID="StudentBirthday" runat="server" Width="107px"></asp:TextBox>&nbsp;
                 <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="StudentBirthday"
                     ErrorMessage="日期格式不正确" Operator="DataTypeCheck" Type="Date" Width="113px"></asp:CompareValidator>
                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="StudentBirthday"
                     ErrorMessage="出生日期必填" Width="186px"></asp:RequiredFieldValidator>
             </td>
         </tr>
         <tr>
             <td class="style8">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;家庭地址:
             </td>
             <td style="width: 33px; height: 10px;">
                 <asp:TextBox ID="StudentAddress" runat="server" Width="283px"></asp:TextBox>
             </td>
         </tr>
         <tr>
             <td class="style10">
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 备&nbsp;&nbsp;
                 注:
             </td>
             <td style="width: 33px; height: 58px;">
                 <asp:TextBox ID="StudentNotes" runat="server" Height="43px" TextMode="MultiLine"
                     Width="283px"></asp:TextBox>
             </td>
         </tr>
         <tr bg>
             <td colspan="2" align="center" style="height: 22px">
                 &nbsp;<asp:Button ID="BtnAdd" runat="server" OnClick="BtnAdd_Click" Text="添加" />
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:Button ID="BtnCancle" runat="server" OnClick="BtnCancle_Click" Text="取消" Style="height: 26px" />
                 &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:HyperLink runat="server" Text="返回首页" NavigateUrl="~/main.aspx"></asp:HyperLink>
                 <br />
                 <br />
                 <asp:Literal ID="ErrMessage" runat="server"></asp:Literal>&nbsp;
             </td>
         </tr>
     </table>
     <asp:SqlDataSource ID="CollegeDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
         ProviderName="System.Data.SqlClient" SelectCommand="SELECT [collegeName] FROM [collegeInfo]">
     </asp:SqlDataSource>
     <asp:SqlDataSource ID="StudentProfessionDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
         ProviderName="System.Data.SqlClient" SelectCommand="SELECT [professionName] FROM [professionInfo] WHERE ([collegeName] = @collegeName)">
         <SelectParameters>
             <asp:ControlParameter ControlID="CollegeName" Name="collegeName" PropertyName="SelectedValue"
                 Type="String" />
         </SelectParameters>
     </asp:SqlDataSource>
 </div>
</form>
</body>
</html>
