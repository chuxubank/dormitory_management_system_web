﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using StudentApartment.BusinessLogicLayer;
using System.Linq;

public partial class AccommodationInfoAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}
        if (!IsPostBack)
        {
            int i;
            for (i = 2005; i <= 2030; i++)
                this.Year.Items.Add(new ListItem(i.ToString(), i.ToString()));
            for (i = 1; i <= 12; i++)
                this.Month.Items.Add(new ListItem(i.ToString(), i.ToString()));
            for (i = 1; i <= 31; i++)
                this.Day.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }


    }
    protected void BtnAdd_Click(object sender, EventArgs e)
    {
        string studentNumber = this.studnetNumber.Text;
        string buildingName = this.buildingName.SelectedValue;
        string roomNo = this.roomNo.SelectedValue ;
        int year = Convert.ToInt32(this.Year.SelectedValue );
        int month = Convert.ToInt32(this.Month.SelectedValue  );
        int day = Convert.ToInt32(this.Day .SelectedValue );
        string  notes = this.accommodationNotes.Text;
        Accommodation l = new Accommodation();
        if (l.AddaccommodationInfo(studentNumber, buildingName, roomNo,year, month, day, notes)==true)
            Response.Write("<script>alert('入住登记成功')</script>");
        else
            Response.Write("<script>alert('"+l.ErrMessage +"')</script>");
        
    }
    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("main.aspx");
    }
}
