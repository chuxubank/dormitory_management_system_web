﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class AccommodationInfoManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}

        if (!IsPostBack)
        {
            ListItem li = new ListItem("请选择楼名", "");
            this.buildingName.Items.Add(li);
            Room room = new Room();
            DataSet ds = room.GetbuildingName();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                DataRow dr = ds.Tables[0].Rows[i];
                li = new ListItem(dr["buildingName"].ToString(), dr["buildingName"].ToString());
                this.buildingName.Items.Add(li);
            }

        }

    }
    protected void checkALL_CheckedChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < this.GridView1.Rows.Count; i++)
        {
            GridViewRow gr = this.GridView1.Rows[i];
            CheckBox chk = (CheckBox)gr.Cells[0].FindControl("checkBox");
            chk.Checked = this.checkALL.Checked; 
        }

    }
 
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            int dataIndex = e.Row.DataItemIndex;
            e.Row.Attributes.Add("onmouseover", "color=this.style.backgroundColor;this.style.backgroundColor='#ccffff';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=color;");

            string studentNumer = e.Row.Cells[1].Text; 
            Literal liveTime = (Literal)e.Row.Cells[5].FindControl("liveInTime");
            liveTime.Text = (new Accommodation()).GetAccommodationTime(studentNumer);  
        }


    }
    protected void BtnLeaveRoom_Click(object sender, EventArgs e)
    {
        bool success = false;
        for (int i = 0; i < this.GridView1.Rows.Count; i++)
        {
            CheckBox chk = (CheckBox)this.GridView1.Rows[i].Cells[0].FindControl("checkbox");
            if (chk.Checked)
            {
                string studentNumber = this.GridView1.Rows[i].Cells[1].Text;
                if((new Accommodation()).CheckoutRoom(studentNumber))
                    success=true;
            }  
        }
        if (success) 
            Response.Write("<script>alert('学生退房成功!');</script>");
        else
            Response.Write("<script>alert('学生退房间失败!');</script>");

        string queryString = "select * from accommodationView where studentNumber like '%" + this.StudentNumber.Text + "%'";
        queryString += " and studentName like '%" + this.StudentName.Text + "%'";
        queryString += " and buildingName like '%" + this.buildingName.SelectedValue + "%'";
        queryString += " and roomNo like '^" + this.roomNo.Text + "%'";
        DataSet ds = (new Accommodation()).GetAccommodationInfo(this.StudentNumber.Text, this.StudentName.Text, this.buildingName.SelectedValue, this.roomNo.Text);
        this.GridView1.DataSourceID = null;
        this.GridView1.DataSource = ds;
        this.GridView1.DataBind();
    }
    protected void BtnOuery__Click(object sender, EventArgs e)
    {
        string queryString = "select * from accommodationView where studentNumber like '%" + this.StudentNumber.Text + "%'";
        queryString += " and studentName like '%" + this.StudentName.Text + "%'";
        queryString += " and buildingName like '%" + this.buildingName.SelectedValue + "%'";
        queryString += " and roomNo like '^" + this.roomNo.Text + "%'";
        DataSet ds = (new Accommodation()).GetAccommodationInfo(this.StudentNumber.Text, this.StudentName.Text, this.buildingName.SelectedValue, this.roomNo.Text);
        this.GridView1.DataSourceID = null;
        this.GridView1.DataSource = ds;
        this.GridView1.DataBind();
    }
}
