<%@ Page Language="C#" AutoEventWireup="true" CodeFile="roomInfoUpdate.aspx.cs" Inherits="roomInfoUpdate" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

    <HEAD>
        <Title></Title>
        <META http-equiv="Content-Type" content="text/html; charset=gb2312">
        <style type="text/css">
            .STYLE1 {
                color: #FF0000
            }

            body,
            td,
            th {
                font-size: 9pt;
            }

            body {
                margin: 20px 0px 0px 0px;
                background-image: url('images/bg.gif');
            }

            .style5 {
                height: 28px;
                text-align: center;
                font-family: 微软雅黑;
            }

            .style6 {
                font-size: large;
            }

            .style7 {
                color: #FFFFFF;
            }
        </style>

    </HEAD>

    <BODY background="images/091dm.jpg">
        <form id="Form1" name=form1 runat=server>
            <div style="border-width: 1px; margin: 0px auto; width: 92%; background-image: url(images/bg_li.gif);
     border-color: #D0E9FF; height: 530px; padding-top: 70px; border-width: 3px; border-style: solid;">
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr bgcolor="#507CD1">
                        <td colspan="2" class="style5">
                            <span class="style6">
                                <b>
                                    <span class="style7">修改房间编号是
                                        <asp:Literal ID="roomNo" runat="server"></asp:Literal>的信息</span>
                                </b>
                            </span>
                            <span class="style7">
                                <font size="-1">(带*号的内容必填！)</font>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px; height: 18px;">
                            所在楼名：
                        </td>
                        <td style="height: 18px">
                            &nbsp;
                            <asp:Literal ID="buildingName" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px; height: 21px;">
                            房间类别：
                        </td>
                        <td style="height: 21px">
                            &nbsp;
                            <asp:DropDownList ID="roomType" runat="server" DataSourceID="roomTypeDataSource" DataTextField="roomTypeName" DataValueField="roomTypeName">
                            </asp:DropDownList>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px; height: 21px">
                            房间价格：
                        </td>
                        <td style="height: 21px">
                            &nbsp;
                            <asp:TextBox ID="roomPrice" runat="server" Width="90px"></asp:TextBox>元/月
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="roomPrice" ErrorMessage="房间价格必填！"
                                Width="139px" ValidationGroup="BtnUpdate"></asp:RequiredFieldValidator>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="roomPrice" ErrorMessage="输入房间价格不正确!" Operator="DataTypeCheck"
                                Type="Currency" ValidationGroup="BtnUpdate"></asp:CompareValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px; height: 26px">
                            总床位数：
                        </td>
                        <td style="height: 26px">
                            &nbsp;
                            <asp:Literal ID="numberOfBed" runat="server"></asp:Literal>个
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px; height: 18px;">
                            现余床位：
                        </td>
                        <td style="height: 18px">
                            &nbsp;
                            <asp:Literal ID="leftNumberOfBed" runat="server"></asp:Literal>个
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px; height: 23px;">
                            宿舍电话：
                        </td>
                        <td style="height: 23px">
                            &nbsp;
                            <asp:TextBox ID="roomTelephone" runat="server" Width="90px"></asp:TextBox>
                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="roomTelephone" ErrorMessage="电话格式不正确!"
                                ValidationExpression="^[0-9]{3,4}-[0-9]{7,8}" Width="116px" ValidationGroup="BtnUpdate"></asp:RegularExpressionValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px">
                            备&nbsp;&nbsp;&nbsp; 注：
                        </td>
                        <td>
                            &nbsp;
                            <asp:TextBox ID="roomNotes" runat="server" Height="43px" TextMode="MultiLine" Width="283px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center" style="height: 22px">
                            <asp:Button ID="BtnUpdate" runat="server" OnClick="BtnUpdate_Click" Text="修改" /> &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;
                            <asp:Button ID="BtnBack" runat="server" Text="返回" OnClick="BtnBack_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </form>
        <asp:SqlDataSource ID="roomTypeDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="System.Data.SqlClient"
            SelectCommand="SELECT [roomTypeName] FROM [roomTypeInfo]">
        </asp:SqlDataSource>

        <br>

    </BODY>

    </HTML>