<%@ Page Language="C#" AutoEventWireup="true" CodeFile="studentInfoUpdate.aspx.cs" Inherits="studentInfoUpdate" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <Title>学生信息更新----学生公寓信息管理系统</Title>
        <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
        <style type="text/css">
            .STYLE1 {
                color: #FF0000
            }

            body,
            td,
            th {
                font-size: 9pt;

            }

            body {
                margin: 20px 0px 0px 0px;
                background-image: url('images/bg.gif');
            }

            .style5 {
                height: 31px;
                text-align: center;
            }

            .style6 {
                font-size: large;
            }

            .style7 {
                color: #FFFFFF;
                font-family: 微软雅黑;
            }

            .style8 {
                width: 120px;
                height: 25px;
            }

            .style9 {
                height: 25px;
            }
        </style>
    </head>

    <body background="images/091dm.jpg">
        <form name="form1" runat="server">
            <div style="border-width: 1px; margin: 0px auto; width: 92%; background-image: url(images/bg_li.gif);
     border-color: #D0E9FF; height: 530px; padding-top: 70px; border-width: 3px; border-style: solid;">
                <table width="600" border="0" cellpadding="0" cellspacing="0" align="center">
                    <tr bgcolor="#507CD1">
                        <td colspan="2" class="style5">
                            <span class="style6">
                                <b>
                                    <span class="style7">修改学号是
                                        <asp:Literal ID="StudentNumber" runat="server"></asp:Literal>的信息</span>
                                </b>
                            </span>
                            <span class="style7">
                                <font size="-1">
                                    (带*号的为必填项)</font>
                            </span>
                            </th>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px">
                            学生姓名:
                        </td>
                        <td>
                            &nbsp;&nbsp;
                            <asp:TextBox ID="StudentName" runat="server" Width="89px"></asp:TextBox>
                            <font color="red">*
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="StudentName"
                                    ErrorMessage="学生姓名必填" Width="110px" ValidationGroup="BtnUpdate"></asp:RequiredFieldValidator>
                            </font>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px">
                            学生性别:
                        </td>
                        <td>
                            &nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="StudentSex" runat="server" Height="16px" Width="90px">
                                <asp:ListItem>男</asp:ListItem>
                                <asp:ListItem>女</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px; height: 21px">
                            政治面貌:
                        </td>
                        <td style="height: 21px">
                            &nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="StudentState" runat="server" Height="16px" Width="90px">
                                <asp:ListItem>团员</asp:ListItem>
                                <asp:ListItem>党员</asp:ListItem>
                                <asp:ListItem>老百姓</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" class="style8">
                            所属学院:
                        </td>
                        <td class="style9">
                            &nbsp;&nbsp;
                            <asp:DropDownList ID="CollegeName" runat="server" AutoPostBack="True" DataSourceID="CollegeDataSource" DataTextField="collegeName"
                                DataValueField="collegeName">
                            </asp:DropDownList>
                            &nbsp;此学生现在学院为:
                            <asp:Literal ID="NowCollegeName" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px">
                            所属专业:
                        </td>
                        <td>
                            &nbsp;&nbsp;
                            <asp:DropDownList ID="StudentProfession" runat="server" DataSourceID="StudentProfessionDataSource" DataTextField="professionName"
                                DataValueField="professionName" Height="18px" Width="90px">
                            </asp:DropDownList>
                            &nbsp;此学生现在专业为:
                            <asp:Literal ID="NowStudentProfession" runat="server"></asp:Literal>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px">
                            出生日期:
                        </td>
                        <td>
                            &nbsp;&nbsp;
                            <asp:TextBox ID="StudentBirthday" runat="server" Width="112px"></asp:TextBox>
                            <span style="color: #ff0000">*</span>
                            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="StudentBirthday"
                                ErrorMessage="日期格式不正确" Operator="DataTypeCheck" Type="Date" Width="113px" ValidationGroup="BtnUpdate"></asp:CompareValidator>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                                ControlToValidate="StudentBirthday" ErrorMessage="出生日期必填" Width="186px" ValidationGroup="BtnUpdate"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px; height: 18px">
                            居住地址:
                        </td>
                        <td style="height: 18px">
                            &nbsp;&nbsp;
                            <asp:TextBox ID="StudentAddress" runat="server" Width="210px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" style="width: 120px">
                            备&nbsp;&nbsp;&nbsp; 注:
                        </td>
                        <td>
                            &nbsp;&nbsp;
                            <asp:TextBox ID="StudentNotes" runat="server" Height="43px" TextMode="MultiLine" Width="283px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center">
                            <br />
                            <asp:Button ID="BtnUpdate" runat="server" OnClick="BtnUpdate_Click" Text="更新" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="BtnBack" runat="server" Text="返回" OnClick="BtnBack_Click" />
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="StudentProfessionDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [professionName] FROM [professionInfo] WHERE ([collegeName] = @collegeName)">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="CollegeName" Name="collegeName" PropertyName="SelectedValue" Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="CollegeDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>" ProviderName="System.Data.SqlClient"
                    SelectCommand="SELECT [collegeName] FROM [collegeInfo]">
                </asp:SqlDataSource>
                <br>
            </div>
        </form>

    </body>

    </html>