﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class PaymentInfoManage : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "color=this.style.backgroundColor;this.style.backgroundColor='#ccffff';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=color;");
            int paymentId = Convert.ToInt32(this.GridView1.DataKeys[e.Row.DataItemIndex][0]);
            Payment money = new Payment();
            e.Row.Cells[6].Text = money.GetPaymentTime(paymentId);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string studentNumber = this.StudentNumber.Text;
        string studentName = this.StudentName.Text;
        string paymentType = this.PaymentType.SelectedValue;

        Payment money = new Payment();
        this.GridView1.DataSourceID = null;
        this.GridView1.DataSource = money.QueryPaymentInfo(studentNumber, studentName, paymentType);
        this.GridView1.DataBind();
    }
    protected void BtnDelete_Click(object sender, EventArgs e)
    {
        bool Success = true;
        for (int i = 0; i < this.GridView1.Rows.Count; i++)
        {
            CheckBox chk = (CheckBox)this.GridView1.Rows[i].Cells[0].FindControl("checkBox");
            if (chk.Checked)
            {
                int paymentId = Convert.ToInt32(this.GridView1.DataKeys[i][0]);
                Payment money = new Payment();
                if(!money.DeletePaymentInfo(paymentId))
                    Success = false;
            }
        }
        if(Success)
            Response.Write("<script>alert('删除缴费信息成功！')</script>");
        else
           Response.Write("<script>alert('删除缴费信息失败！')</script>");
    }
    protected void checkALL_CheckedChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < this.GridView1.Rows.Count; i++)
        {
            GridViewRow gr = this.GridView1.Rows[i];
            CheckBox chk = (CheckBox)gr.Cells[0].FindControl("checkBox");
            chk.Checked = this.checkALL.Checked; 
        }
    }
}
