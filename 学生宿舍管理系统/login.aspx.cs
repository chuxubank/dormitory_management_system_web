﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
  
    protected void Button1_Click(object sender, EventArgs e)
    {
        Admin admin = new Admin();
        admin.AdminUserName = txtName.Text;
        admin.AdminPassword = txtPwd.Text;

        if (admin.checkAdmin()) 
        {
            Session["adminUserName"] = txtName.Text;
            Response.Redirect("main.aspx");         
        }
        else
        {
            this.ErrMessage.Text = "" + admin.ErrMessage + "";
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        txtName.Text = "";
        txtPwd .Text ="";
    }
}
