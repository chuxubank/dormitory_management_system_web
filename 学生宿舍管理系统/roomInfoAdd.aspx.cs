﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class roomInfoAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}
    }
    protected void BtnAdd_Click(object sender, EventArgs e)
    {
        Room room = new Room();
        room.RoomNo = this.roomNo.Text;
        room.BuildingName = this.buildingName.SelectedValue;
        room.RoomType = this.roomType.Text;
        room.RoomPrice = Convert.ToSingle(this.roomPrice.Text);
        room.NumberOfBed = Convert.ToInt16(this.numberOfBed.Text);
        room.LeftNumberOfBed = Convert.ToInt16(this.leftNumberOfBed.Text);
        room.RoomTelephone = this.roomTelephone.Text;
        room.RoomNotes = this.roomNotes.Text;

        if (room.InsertRoomInfo()) 
            this.ErrMessage.Text = "房间信息添加成功";
        else
            this.ErrMessage.Text = room.ErrMessage ; 
    }
}
