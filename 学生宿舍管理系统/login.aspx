<%@ Page Language="C#" AutoEventWireup="true" CodeFile="login.aspx.cs" Inherits="login" %>

<%@ Register Src="UserControl/WebHeadControl.ascx" TagName="WebHeadControl" TagPrefix="uc1" %>

<!DOCTYPE html PUBLIC "-//W3C//Dtd XHTML 1.0 transitional//EN" "http://www.w3.org/tr/xhtml1/Dtd/xhtml1-transitional.dtd">

<html>
<head>
		<title>学生宿舍管理系统</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR" />
		<meta content="C#" name="CODE_LANGUAGE" />
		<meta content="JavaScript" name="vs_defaultClientScript" />
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema" />
		<style type="text/css">
            body
            {
            	background-image:url('images/bbsinfo.jpg');	
            	margin-top:0px;
            }
            .tableTitle
            {
                font-size: 15px;font-family:微软雅黑;font-weight:bold;
            }
            .tableFontStyle
            {
                font-size: 12px;
            }
            
           
  
        </style>
    </head>
	<body>
	
	  <div style="margin:-5px auto; width:980px; height:auto; ">
		<form id="Form1" method="post" runat="server">
		    <div style="width:700px; height:609px; margin:0px auto; background:url('images/register.gif') no-repeat 0px;">
		        <div style="float:left; margin-left:55px; margin-top:15px; height: 65px; width: 523px;"><img src="images/title.png" /></div>
		        
                <table id="table2" cellspacing="0" style="border-collapse: collapse; margin-top:200px;
                    border: solid 1px #9933cc;" cellpadding="0" width="280" align="center" bgcolor="#ccffff"
                    border="1">
                    <tr>
                        <td align="center" colspan="2" align="center" bgcolor="#CCCCFF" class="tableTitle">
                            登录
                        </td>
                    </tr>
                    <tr>
                    <td style="height: 33px" align="right" width="41%" class="tableFontStyle">
                        <img src="images/IcoUsername.gif" />
                        用户名：
                    </td>
                    <td style="height: 33px" align="left" width="59%">
                        <asp:TextBox ID="txtName" runat="server" MaxLength="20" Width="145"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                            ErrorMessage="用户名必填" ControlToValidate="txtName" Font-Size="Small" ValidationGroup="Button1"></asp:RequiredFieldValidator>
                    </td>
                    </tr>
                    <tr>
                        <td align="right" width="41%" class="tableFontStyle">
                            <img src="images/IcoSign.gif" />
                            密码：
                        </td>
                        <td align="left">
                            <asp:TextBox ID="txtPwd" runat="server" MaxLength="30" Width="145" Height="15" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" Display="Dynamic"
                                ErrorMessage="密码必填" ControlToValidate="txtPwd" Font-Size="Small" ValidationGroup="Button1"></asp:RequiredFieldValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2" height="40">
                            &nbsp;
                            <asp:Button ID="Button1" Style="margin-right: 25px" runat="server" Text="登录" OnClick="Button1_Click">
                            </asp:Button>
                            <asp:Button ID="Button2" runat="server" Text="取消" OnClick="Button2_Click" />
                        </td>
                    </tr>
                </table>
                 <asp:Literal ID="ErrMessage" runat="server"></asp:Literal> 
            </div>     
		</form>
		</div>
	</body>
</html>