<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AccommodationInfoManage.aspx.cs" Inherits="AccommodationInfoManage" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head>
        <Title></Title>
        <META http-equiv="Content-Type" content="text/html; charset=gb2312">
        <style type="text/css">
            .STYLE1 {
                color: #FF0000
            }

            body,
            td,
            th {
                font-size: 9pt;
            }

            body {
                margin: 20px 0px 0px 0px;
                background-image: url('images/bg.gif');
            }

            .style5 {
                font-size: large;
                font-weight: bold;
                text-align: center;
                color: #FFFFFF;
                height: 23px;
                font-family: 微软雅黑;
            }

            .style6 {
                text-align: center;
            }
        </style>
    </head>

    <body>
        <form id=form1 runat="server">
            <div style="border-width:1px; margin:0px auto; width:92%; background-image:url(images/bg_li.gif); border-color:#D0E9FF; height:530px; padding-top:70px; border-width:3px; border-style:solid;">
                <table width="100%" border='1' align="center" cellpadding='0' cellspacing='0' bordercolor="#F0F0F0" id="TabDocMain">
                    <tr bgcolor="#507CD1">
                        <td class="style5">
                            管理住宿信息
                        </td>
                    </tr>
                    <tr>
                        <td colspan="9" style="height: 19px" class="style6">
                            <div class="style6">
                                学号:
                                <asp:TextBox ID="StudentNumber" runat="server" Width="75px"></asp:TextBox>姓名:
                                <asp:TextBox ID="StudentName" runat="server" Width="72px"></asp:TextBox>所在楼名:
                                <asp:DropDownList ID="buildingName" runat="server">
                                </asp:DropDownList>
                                房间编号:
                                <asp:TextBox ID="roomNo" runat="server" Width="72px"></asp:TextBox>&nbsp;
                                <asp:Button ID="BtnQuery" runat="server" Text="查询" OnClick="BtnOuery__Click" />
                                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/main.aspx">返回首页</asp:HyperLink>
                                <br />
                            </div>
                            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False" BackColor="White" BorderColor="#CCCCCC"
                                BorderStyle="None" BorderWidth="1px" CellPadding="3" DataSourceID="accommodationDataSource" OnRowDataBound="GridView1_RowDataBound"
                                Width="100%" DataKeyNames="studentNumber,roomNo">
                                <FooterStyle BackColor="White" ForeColor="#000066" />
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:CheckBox ID="checkBox" runat="server" />
                                        </ItemTemplate>
                                        <ControlStyle Width="30px" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="studentNumber" HeaderText="学生学号" SortExpression="studentNumber" />
                                    <asp:BoundField DataField="studentName" HeaderText="学生姓名" SortExpression="studentName" />
                                    <asp:BoundField DataField="buildingName" HeaderText="所属楼名" SortExpression="buildingName" />
                                    <asp:BoundField DataField="roomNo" HeaderText="房间编号" SortExpression="roomNo" />
                                    <asp:TemplateField HeaderText="入住日期">
                                        <ItemTemplate>
                                            <asp:Literal ID="liveInTime" runat="server"></asp:Literal>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="accommodationNotes" HeaderText="备注" SortExpression="accommodationNotes" />
                                    <asp:HyperLinkField DataNavigateUrlFields="studentNumber" DataNavigateUrlFormatString="AccommodationinInfoUpdate.aspx?studentNumber={0}"
                                        HeaderText="操作" Text="换房" />
                                </Columns>
                                <RowStyle ForeColor="#000066" />
                                <EmptyDataTemplate>
                                    抱歉，您要查询的信息不存在!
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                <PagerStyle ForeColor="#000066" HorizontalAlign="Left" BackColor="White" />
                                <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                            </asp:GridView>
                            <asp:CheckBox ID="checkALL" runat="server" AutoPostBack="True" OnCheckedChanged="checkALL_CheckedChanged" Text=" 全选" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="BtnLeaveRoom" runat="server" OnClick="BtnLeaveRoom_Click" Text="退房" Style="height: 26px" />
                            <br />
                            <asp:SqlDataSource ID="accommodationDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                                ProviderName="System.Data.SqlClient" SelectCommand="SELECT [studentName], [accommodationinYear], [studentNumber], [accommodationinMonth], [accommodationinDay], [accommodationNotes], [buildingName], [roomNo] FROM [accommodationView]">
                            </asp:SqlDataSource>
                        </td>
                    </tr>
                </table>
                <asp:SqlDataSource ID="buildingNameDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:ConnectionString %>"
                    ProviderName="System.Data.SqlClient" SelectCommand="SELECT [buildingName] FROM [buildingInfo]">
                </asp:SqlDataSource>
            </div>
        </form>
    </body>

    </html>