﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.DataAccessLayer;
using StudentApartment.DataAccessHelper;

public partial class ProfessionSet : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}
    }
    protected void checkALL_CheckedChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < this.GridView1.Rows.Count; i++)
        {
            GridViewRow gr = this.GridView1.Rows[i];
            CheckBox chk = (CheckBox)gr.Cells[0].FindControl("checkBox");
            chk.Checked = this.checkALL.Checked; 
        }
    }
   
    protected void BtnAdd_Click(object sender, EventArgs e)
    {
        string collegeName = this.CollegeName.SelectedValue;
        string professionName = this.professionName.Text;
        if (professionName == "")
        {
            Response.Write ("<script>alert('专业信息必填!');</script>");
            return;
        }
        string queryString = "select * from professionInfo where professionName=" + SqlString.GetQuotedString(professionName);
        DataBase db = new DataBase();
        if (db.GetRecord(queryString))
        {
            Response.Write("<script>alert('此专业已存在！');</script>");
            return;
        }

        string insertString = "insert into professionInfo(professionName,collegeName) values (";
        insertString += SqlString.GetQuotedString(professionName) + ",";
        insertString += SqlString.GetQuotedString(collegeName) + ")";
        if (db.InsertOrUpdate(insertString)<0)
            Response.Write("<script>alert('专业添加失败！');</script>");
        Response.Write("<script>alert('专业添加成功！');</script>");

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "color=this.style.backgroundColor;this.style.backgroundColor='#ccffff';");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=color;");
        }
    }
    protected void BtnDelete_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow gr in GridView1.Rows)
        {
            CheckBox chk = (CheckBox)gr.Cells[0].FindControl("checkBox");
            if (chk.Checked) 
            {
                string professionName = gr.Cells[1].Text;
                string queryString = "select * from studentInfo where studentProfession=" + SqlString.GetQuotedString(professionName);
                DataBase db = new DataBase();
                if (db.GetRecord(queryString))
                    Response.Write("<script>alert('此专业还有学生，不能删除!');</script>");
                else
                {
                    string deleteString = "delete from professionInfo where professionName=" + SqlString.GetQuotedString(professionName);
                    db.InsertOrUpdate(deleteString);
                    Response.Write("<script>alert('专业删除成功!')</script>");
                }

            }
        }
    }
   
}
