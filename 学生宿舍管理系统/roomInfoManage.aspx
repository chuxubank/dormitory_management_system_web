﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="roomInfoManage.aspx.cs" Inherits="roomInfoManage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head>
<title></title>
<style type="text/css">
.STYLE1 {color: #FF0000}
body,td,th {
	font-size: 9pt;
}
body 
{
	margin:0px 0px 0px 0px;
	background-image:url('images/bg.gif');
}
    .style5
    {
        font-size: large;
        font-weight: bold;
        text-align: center;
        font-family:微软雅黑;
        color: #FFFFFF;
        height: 23px;
    }
    .style6
    {
        text-align: center;
    }
    </style>
</head>
<body background="images/091dm.jpg">
<form method="POST" id="form1"  runat="server">

<div style="border-width:1px; margin:0px auto; width:92%; background-image:url(images/bg_li.gif); border-color:#D0E9FF; height:530px; padding-top:70px; border-width:3px; border-style:solid;">
    <table width="95%" align="center" cellpadding='0' cellspacing='0' style="border:solid 1px #0066cc;" id="TabDocMain">
		 <tr bgcolor="#507CD1">
		<td class="style5">查询房间信息</td>
	  </tr>
	  <tr>
           <td colspan=8 class="style6">
               <div class="style6">
               房间编号:<asp:TextBox ID="RoomNo" runat="server" Width="71px"></asp:TextBox>
                   所在楼名:<asp:DropDownList ID="buildingName" runat="server">
               </asp:DropDownList>
               房间类别:<asp:DropDownList ID="roomType" runat="server">
               </asp:DropDownList>&nbsp;
               <asp:Button ID="BtnQuery" runat="server" OnClick="BtnQuery_Click" Text="查询" />&nbsp;<asp:HyperLink 
                       ID="HyperLink1" runat="server" NavigateUrl="~/main.aspx">返回首页</asp:HyperLink>
                   <br />
               </div>
               <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                   BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                   CellPadding="3"  OnRowDataBound="GridView1_RowDataBound"
                   Width="100%">
                   <FooterStyle BackColor="White" ForeColor="#000066" />
                   <Columns>
                       <asp:TemplateField HeaderText="">
                          <ItemTemplate>
                          <asp:CheckBox ID="checkBox" runat=server /></ItemTemplate>
                            <ControlStyle Width="30px" />
                        </asp:TemplateField>
                       <asp:BoundField DataField="roomNo" HeaderText="房间编号" SortExpression="roomNo" />
                       <asp:BoundField DataField="buildingName" HeaderText="所属楼名" SortExpression="buildingName" />
                       <asp:BoundField DataField="roomType" HeaderText="房间类别" SortExpression="roomType" />
                       <asp:BoundField DataField="roomPrice" HeaderText="房间价格" SortExpression="roomPrice" />
                       <asp:BoundField DataField="leftNumberOfBed" HeaderText="剩余床位" SortExpression="leftNumberOfBed" />
                       <asp:HyperLinkField DataNavigateUrlFields="roomNo" DataNavigateUrlFormatString="roomInfoUpdate.aspx?roomNo={0}"
                           HeaderText="操作" Text="详情" />
                   </Columns>
                   <RowStyle ForeColor="#000066" />
                   <EmptyDataTemplate>
                      抱歉，您要查询的信息不存在!
                  </EmptyDataTemplate>
                   <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                   <PagerStyle ForeColor="#000066" HorizontalAlign="Left" BackColor="White" />
                   <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
               </asp:GridView>
               <div style=" margin-top:10px; padding-bottom:10px;">
               <asp:CheckBox ID="CheckALL" runat="server" AutoPostBack="True" OnCheckedChanged="CheckALL_CheckedChanged"
                   Text=" 全选" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:Button 
                         ID="BtnDel" runat="server"
                       Text="删除" onclick="BtnDel_Click" />
               </div>        
           </td>
          </tr>	
</table>
</div>
</form>
</body>
</html>
		
		
	
