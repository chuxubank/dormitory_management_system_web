﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class studentInfoUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        String studentNumber = Request.QueryString["StudentNumber"]; //取得学号信息参数
        this.StudentNumber.Text = studentNumber;

        Student student = new Student();
        student.QueryStudentInfo(studentNumber); 

        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}

        if (!IsPostBack)
        {
            this.StudentName.Text = student.StudentName;
            this.StudentSex.SelectedValue = student.StudentSex;
            this.StudentState.SelectedValue = student.StudentState;
         

            this.CollegeName.Text = student.CollegeName;
            this.StudentProfession.Text = student.StudentProfession;
            this.StudentBirthday.Text = student.StudentBirthday.ToShortDateString();
            this.StudentAddress.Text = student.StudentAddress;
            this.StudentNotes.Text = student.StudentNotes;

            this.NowCollegeName.Text =  student.CollegeName ;
            this.NowStudentProfession.Text = student.StudentProfession;
        }      
    }
    protected void BtnUpdate_Click(object sender, EventArgs e)
    {
        Student student = new Student();
        student.StudentNumber = this.StudentNumber.Text;
        student.StudentName = this.StudentName.Text;
        student.StudentSex = this.StudentSex.SelectedValue;
        student.StudentState = this.StudentState.SelectedValue; ;
        student.CollegeName = this.CollegeName.SelectedValue;
        student.StudentProfession = this.StudentProfession.SelectedValue;
        student.StudentBirthday = Convert.ToDateTime(this.StudentBirthday.Text);
        student.StudentAddress = this.StudentAddress.Text;
        student.StudentNotes = this.StudentNotes.Text;

        if(student.UpdateStudetnInfo())
            Response.Write("<script>alert('学生信息更新成功!');location.href='studentInfoUpdate.aspx?studentNumber=" + student.StudentNumber + "';</script>");
    }
    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("studentInfoManage.aspx");
    }
    
}
