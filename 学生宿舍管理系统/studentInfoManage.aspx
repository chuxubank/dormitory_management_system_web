<%@ Page Language="C#" AutoEventWireup="true" CodeFile="studentInfoManage.aspx.cs"
    Inherits="studentInfoManage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=gb2312" />
    <style type="text/css">
        .STYLE1
        {
            color: #FF0000;
        }
        body, td, th
        {
            font-size: 9pt;
        }
        body
        {
            margin: 20px 0px 0px 0px;
            background-image: url( 'images/bg.gif' );
        }
        .style5
        {
            font-size: large;
            font-weight: bold;
            text-align: center;
            color: #FFFFFF;
            height: 18px;
            font-family: 微软雅黑;
        }
        .style6
        {
            text-align: center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div style="border-width: 1px; margin: 0px auto; width: 92%; background-image: url(images/bg_li.gif);
        border-color: #D0E9FF; height: 530px; padding-top: 70px; border-width: 3px; border-style: solid;">
        <table width="100%" border="0" cellpadding="0" cellspacing="2" align="center" style="margin-bottom: 5px">
            <tr bgcolor="#507CD1">
                <td class="style5">
                    查询学生信息
                </td>
            </tr>
        </table>
        <div>
            <table width="100%" border="0" cellpadding="0" cellspacing="2" align="center">
                <tr>
                    <td height="21" class="style6">
                        <div class="style6">
                            学生学号：<asp:TextBox ID="StudentNumber" runat="server" Width="91px"></asp:TextBox>
                            学生姓名：<asp:TextBox ID="StudentName" runat="server" Width="80px"></asp:TextBox>&nbsp;
                            所属专业<asp:DropDownList ID="StudentProfession" runat="server">
                            </asp:DropDownList>
                            &nbsp;<asp:Button ID="BtnQuery" runat="server" OnClick="BtnQuery_Click" Text="查询" />&nbsp;<asp:HyperLink
                                ID="HyperLink1" runat="server" NavigateUrl="~/main.aspx">返回首页</asp:HyperLink>
                            <br />
                        </div>
                        <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                            BackColor="White" BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px"
                            CellPadding="3" Width="100%" OnRowDataBound="GridView1_RowDataBound">
                            <FooterStyle BackColor="White" ForeColor="#000066" />
                            <Columns>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:CheckBox ID="checkBox" runat="server" /></ItemTemplate>
                                    <ControlStyle Width="30px" />
                                </asp:TemplateField>
                                <asp:BoundField DataField="studentNumber" HeaderText="学生学号" SortExpression="studentNumber">
                                    <ControlStyle Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="studentName" HeaderText="学生姓名" SortExpression="studentName">
                                    <ControlStyle Width="100px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="studentSex" HeaderText="学生性别" SortExpression="studentSex">
                                    <ControlStyle Width="20px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="studentState" HeaderText="政治面貌" SortExpression="studentState">
                                    <ControlStyle Width="50px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="studentProfession" HeaderText="所属专业" SortExpression="studentProfession">
                                    <ControlStyle Width="60px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="studentBirthday" HeaderText="出生日期" SortExpression="studentBirthday">
                                    <ControlStyle Width="30px" />
                                </asp:BoundField>
                                <asp:HyperLinkField DataNavigateUrlFields="studentNumber" DataNavigateUrlFormatString="studentInfoUpdate.aspx?studentNumber={0}"
                                    HeaderText="操作" Text="更新">
                                    <ControlStyle Width="30px" />
                                </asp:HyperLinkField>
                            </Columns>
                            <RowStyle ForeColor="#000066" />
                            <EmptyDataTemplate>
                                抱歉，您要查询的信息不存在!
                            </EmptyDataTemplate>
                            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                            <PagerStyle ForeColor="#000066" HorizontalAlign="Left" BackColor="White" />
                            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                        </asp:GridView>
                        <asp:CheckBox ID="checkALL" runat="server" Text=" 全选" OnCheckedChanged="checkALL_CheckedChanged"
                            AutoPostBack="True" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Button1" runat="server" Text="删除" OnClick="Button1Click" />
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
    </div>
    </form>
</body>
</html>
