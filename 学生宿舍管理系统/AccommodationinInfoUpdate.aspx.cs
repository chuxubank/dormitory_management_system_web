﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class AccommodationinInfoUpdate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}

        string studentNumber = Request.QueryString["studentNumber"];
        this.StudentNumber.Text = studentNumber;

        if (!IsPostBack)
        {
            int i;
            for (i = 2005; i <= 2030; i++)
                this.Year.Items.Add(new ListItem(i.ToString(), i.ToString()));
            for (i = 1; i <= 12; i++)
                this.Month.Items.Add(new ListItem(i.ToString(), i.ToString()));
            for (i = 1; i <= 31; i++)
                this.Day.Items.Add(new ListItem(i.ToString(), i.ToString()));
            Accommodation live = new Accommodation();
            live.QueryAccommodationInfo(studentNumber); 
            this.buildingName.Text = live.BuildingName;
            this.Now_roomNo.Text =live.RoomNo;
            this.buildingName1.Text = live.BuildingName;
            this.roomNo.Text = live.RoomNo;
            this.Year.Text = live.AccommodationinYear.ToString();
            this.Month.Text = live.AccommodationinMonth.ToString();
            this.Day.Text = live.AccommodationinDay.ToString();
            this.accommodationNotes.Text = live.AccommodationNotes;
        }






    }
    protected void BtnChangeRoom_Click(object sender, EventArgs e)
    {
        Accommodation live = new Accommodation();
        live.StudentNumber = Request.QueryString["studentNumber"];
        live.BuildingName = this.buildingName.SelectedValue;
        live.RoomNo = this.roomNo.SelectedValue;
        live.AccommodationinYear = Convert.ToInt16(this.Year.SelectedValue);
        live.AccommodationinMonth = Convert.ToInt16(this.Month.SelectedValue);
        live.AccommodationinDay = Convert.ToInt16(this.Day.SelectedValue);
        live.AccommodationNotes = this.accommodationNotes.Text;

        if (live.UpdateAccommodationInfo())
            Response.Write("<script>alert('房间更换成功!');location.href='AccommodationinInfoUpdate.aspx?studentNumber=" + live.StudentNumber + "';</script>");
        else
            Response.Write("<script>alert('" + live.ErrMessage + "');</script>");

    }
    protected void BtnBack_Click(object sender, EventArgs e)
    {
        Response.Redirect("AccommodationInfoManage.aspx");
    }

}
