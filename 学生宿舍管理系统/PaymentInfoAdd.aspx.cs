﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Linq;
using StudentApartment.BusinessLogicLayer;

public partial class PaymentInfoAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (Session["adminUserName"] == null)
        //{
        //    Response.Redirect("login.aspx");
        //}
        if (!IsPostBack)
        {
            int i;
            for (i = 2005; i <= 2030; i++)
                this.PaymentYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            for (i = 1; i <= 12; i++)
                this.PaymentMonth.Items.Add(new ListItem(i.ToString(),i.ToString()));
            for (i = 1; i <= 31; i++)
                this.PaymentDay.Items.Add(new ListItem(i.ToString(),i.ToString()));
        }

    }
    protected void BtnAdd_Click(object sender, EventArgs e)
    {
        Payment money = new Payment();
        money.StudentNumber = this.StudentNumber.Text;
        money.PaymentType = this.PaymentType.SelectedValue ;
        money.PaymentYear = Convert.ToInt16(this.PaymentYear.SelectedValue);
        money.PaymentMonth = Convert.ToInt16(this.PaymentMonth.SelectedValue);
        money.PaymentDay = Convert.ToInt16(this.PaymentDay.SelectedValue);
        money.GiveMoney = Convert.ToSingle(this.Price.Text);

        if (money.InsertPayment())
            this.ErrMessage.Text = "缴费成功!";
        else
            this.ErrMessage.Text = money.ErrMessage;

    }
    protected void BtnCancle_Click(object sender, EventArgs e)
    {
        Response.Redirect("main.aspx");
    }
   
}
